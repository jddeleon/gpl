#ifndef VARIABLE_H
#define VARIABLE_H

#include <string>
#include "symbol.h"
#include "symbol_table.h"
#include "gpl_type.h"
#include "expression.h"
#include "animation_block.h"
#include "game_object.h"
#include <cassert>
#include <sstream>

class Expr;

class Variable {
    public:
        enum Var_kind {SIMPLE,
                       ARRAY,
                       MEMBER,
                       MEMBER_ARRAY}; // so far haven't use this. should I?
       
        Variable(Symbol* symbol);
        Variable(string id, Expr* expr, Gpl_type type);
        Variable(string id);
        Variable(Game_object* object, string id, string member);
        Variable(Game_object* object, string id, Expr* expr, string member);
        
        void                set_id(string id) {m_id = id;}
        void                set_type(Gpl_type type) {m_type = type;}
        
        Gpl_type            get_type() {return m_type;}
        string              get_id() {return m_id;}
        int                 get_int_val();
        double              get_double_val();
        string              get_string_val();
        Animation_block*    get_animation_block() {assert(m_type == ANIMATION_BLOCK); return m_animation_block;}
        Symbol*             get_symbol();
        Var_kind            get_kind() {return m_kind;}
        string              get_member_id() {return m_member_id;}

    private:
        string              m_id;
        Symbol             *m_symbol;
        Gpl_type            m_type;
        Var_kind            m_kind; // Simple (i), array i[x*y], member (my_circle.x)
        Expr               *m_expr; // ex. nums[x + 3]
        int                 m_int_val;
        double              m_double_val;
        string              m_string_val;
        Animation_block    *m_animation_block;
        string              m_member_id;
       

};

#endif
