#include "variable.h"

// Constructor for SIMPLE variable
Variable::Variable(Symbol* symbol)
{
    assert(symbol != NULL);
    m_symbol = symbol;
    m_id = symbol->get_id();
    m_type = symbol->get_type();
    m_expr = NULL;

    if (m_type == INT)
    {
        m_int_val = m_symbol->get_int_val();
    }
    else if (m_type == DOUBLE)
    {
        m_double_val = m_symbol->get_double_val();
    }
    else
    {
        assert(m_type == STRING);
        m_string_val = m_symbol->get_string_val();
    } 
}

// Constructor for ARRAY variable
Variable::Variable(Expr* expr)
{
    assert(expr != NULL);
    m_expr = expr;
    m_type = m_expr->get_type();
    m_symbol = NULL;
}


