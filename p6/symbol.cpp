#include "symbol.h"

Symbol::Symbol(string name, int val)
{
    m_type = INT;
    m_int_val = val;
    m_id = name;
    m_game_object = NULL;
    m_animation_block = NULL;

}

Symbol::Symbol(string name, double val)
{
    m_type = DOUBLE;
    m_double_val = val;
    m_id = name;
    m_game_object = NULL;
    m_animation_block = NULL;

}

Symbol::Symbol(string name, string val)
{
    m_type = STRING;
    m_string_val = val;
    m_id = name;
    m_game_object = NULL;
    m_animation_block = NULL;
}

Symbol::Symbol(string name, Game_object *game_object)
{
    m_type = GAME_OBJECT;
    m_id = name;
    m_game_object = game_object;
    m_animation_block = NULL;
}

Symbol::Symbol(string name, Animation_block* animation_block)
{
    m_type = ANIMATION_BLOCK;
    m_id = name;
    m_animation_block = animation_block;
    m_game_object = NULL;
}

void Symbol::print(ostream &os)
{
    int type = m_type;
    switch (type)
    {
        case INT:
            os << "int " << m_id << " " << m_int_val << endl;
            break;
        case DOUBLE:
            os << "double " << m_id << " " << m_double_val << endl;
            break;
        case STRING:
            os << "string " << m_id << " \"" << m_string_val << "\"" << endl;
            break;
        case GAME_OBJECT:
            os << indent << "game_object " << m_id << endl;
            indent++;
            m_game_object->print(os);
            indent--;
            os << endl;
            break;
        case ANIMATION_BLOCK:
            os << indent << "animation_block " << m_id << endl;
            indent++;
            m_animation_block->print(os);
            indent--;
            os << endl;
            break;
        default:
            assert(false);
            
    }    
}
