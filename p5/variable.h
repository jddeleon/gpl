#ifndef VARIABLE_H
#define VARIABLE_H

#include <string>
#include "symbol.h"
#include "gpl_type.h"
#include "expression.h"
#include <cassert>

class Expr;

class Variable {
    public:
        enum Var_kind {SIMPLE,
                       ARRAY,
                       MEMBER};
       
                    Variable(Symbol* symbol);
                    Variable(Expr* expr);
                    Variable(string id);
        string      get_id() {return m_id;}
        void        set_id(string id) {m_id = id;}
        Gpl_type    get_type() {return m_type;}
        void        set_type(Gpl_type type) {m_type = type;}
        int         get_int_val() {assert(m_type == INT); return m_int_val;}
        double      get_double_val() {assert(m_type == DOUBLE); return m_double_val;}
        string      get_string_val() {assert(m_type == STRING); return m_string_val;}

    private:
        string      m_id;
        Symbol     *m_symbol;
        Gpl_type    m_type;
        // int         m_kind; // Simple (i), array i[x*y], member (my_circle.x)
        Expr       *m_expr; // ex. nums[x + 3]
        int         m_int_val;
        double      m_double_val;
        string      m_string_val;
       

};

#endif
