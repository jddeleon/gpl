#include "symbol_table.h"

Symbol_table *Symbol_table::m_instance = 0;

Symbol_table *Symbol_table::instance()
{
    if (m_instance == 0)
    {
        m_instance = new Symbol_table();
    }
    return m_instance;
}

void Symbol_table::print(ostream &os)
{
    for (m_iter = m_sym_map.begin(); m_iter != m_sym_map.end(); m_iter++)
    {
        m_iter->second->print(os);
    }
}

bool Symbol_table::insert(string name, Symbol *symbol)
{
    if (this->lookup(name) != NULL)
    {
        return false;
    }
    m_sym_map[name] = symbol;
    return true;
}

Symbol* Symbol_table::lookup(string name)
{
   
    if (m_sym_map.find(name) == m_sym_map.end() && m_sym_map.find(name+"[0]") == m_sym_map.end())
    {
        return NULL;
    }
    m_iter = m_sym_map.find(name);
    return m_iter->second;
}

bool Symbol_table::get(string name, int &value)
{
    Symbol *cur = lookup(name);
    
    // if the symbol is not in the symbol_table or the type is not an INT
    if (!cur || cur->get_type() != INT)
    {
        return false;
    }
    else
    {
        value = cur->get_int_val();
        return true;
    }
}

bool Symbol_table::get(string name, double &value)
{
    Symbol *cur = lookup(name);
    
    // if the symbol is not in the symbol_table or the type is not a DOUBLE
    if (!cur || cur->get_type() != DOUBLE)
    {
        return false;
    }
    else
    {
        value = cur->get_double_val();
        return true;
    }
}

bool Symbol_table::get(string name, string &value)
{
    Symbol *cur = lookup(name);
    
    // if the symbol is not in the symbol_table or the type is not an STRING
    if (!cur || cur->get_type() != STRING)
    {
        return false;
    }
    else
    {
        value = cur->get_string_val();
        return true;
    }

}

bool Symbol_table::get(string name, Animation_block *animation_block_ptr)
{
    Symbol *cur = lookup(name);

    if (!cur || cur->get_type() != ANIMATION_BLOCK)
    {
        return false;
    }
    else
    {
        animation_block_ptr = cur->get_animation_block();
        return true;
    }
}

bool Symbol_table::get_type(string name, Gpl_type &type)
{
    Symbol *cur = lookup(name);

    if (!cur)
    {
        return false;
    }
    else
    {
        type = cur->get_type();
        return true;
    }
}

bool Symbol_table::set(string name, int value)
{
    
    Symbol *cur = lookup(name);

    if (!cur)
    {
        return false;
    }
    else
    {
        cur->set_int_val(value);
        return true;
    }
}




