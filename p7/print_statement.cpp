#include "print_statement.h"


PrintStmt::PrintStmt(Expr *expr, int line_no)
{
    m_expr = expr;
    m_line_no = line_no;
}

void PrintStmt::execute()
{
    assert(m_expr != NULL);
    cout << "gpl[" << m_line_no << "]: "; 
    switch (m_expr->get_type())
    {
        case INT:
            cout << m_expr->eval_int();
            break;
        case DOUBLE:
            cout << m_expr->eval_double();
            break;
        case STRING:
            cout << m_expr->eval_string();
            break;
        default:
            assert(false);

    }
    cout << endl;
       
}
