#ifndef ASSIGNMENT_STATEMENT_H
#define ASSIGNMENT_STATEMENT_H

#include "statement.h"
#include "variable.h"
#include "gpl_type.h"

class AssignmentStmt : public Statement
{
    public:
        AssignmentStmt(Variable *lhs, Expr *rhs, Operator_type op);

        virtual void execute();

    private:
        Variable       *m_lhs;
        Expr           *m_rhs;
        Operator_type   m_op;
};

#endif
