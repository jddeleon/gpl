#include "variable.h"

// Constructor for SIMPLE variable
Variable::Variable(Symbol* symbol)
{
    assert(symbol != NULL);
    m_symbol = symbol;
    m_id = symbol->get_id();
    m_type = symbol->get_type();
    m_expr = NULL;

    if (m_type == INT)
    {
        m_int_val = m_symbol->get_int_val();
    }
    else if (m_type == DOUBLE)
    {
        m_double_val = m_symbol->get_double_val();
    }
    else if (m_type == STRING)
    {
        m_string_val = m_symbol->get_string_val();
    }
    else
    {
        assert(m_type == ANIMATION_BLOCK);
        m_animation_block = m_symbol->get_animation_block();
    }
}

// Constructor for ARRAY variable
Variable::Variable(Expr* expr)
{
    assert(expr != NULL);
    m_expr = expr;
    m_type = m_expr->get_type();
    m_symbol = NULL;
}

Variable::Variable(Game_object* object, string member)
{
    assert(object != NULL);
    m_id = member;
    Status object_status;
    
    object_status = object->get_member_variable_type(member, m_type);
    assert(object_status == OK);

    if (m_type == INT)
        object_status = object->get_member_variable(member, m_int_val);
    else if (m_type == DOUBLE)
        object_status = object->get_member_variable(member, m_double_val);
    else
    {
        assert(m_type == STRING);
        object_status = object->get_member_variable(member, m_string_val);
    }

    assert(object_status == OK);


}
