#include "symbol.h"

Symbol::Symbol(string name, int val)
{
    m_type = INT;
    m_int_val = val;
    m_id = name;

}

Symbol::Symbol(string name, double val)
{
    m_type = DOUBLE;
    m_double_val = val;
    m_id = name;

}

Symbol::Symbol(string name, string val)
{
    m_type = STRING;
    m_string_val = val;
    m_id = name;
}

void Symbol::print(ostream &os)
{
    int type = m_type;
    switch (type)
    {
        case INT:
            os << "int " << m_id << " " << m_int_val << endl;
            break;
        case DOUBLE:
            os << "double " << m_id << " " << m_double_val << endl;
            break;
        case STRING:
            os << "string " << m_id << " " << m_string_val << endl;
            break;
    }    
}
