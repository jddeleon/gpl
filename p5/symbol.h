#ifndef SYMBOL_H
#define SYMBOL_H

#include <iostream>
#include <string>
#include <fstream>

using namespace std;
#include "gpl_type.h"

class Symbol
{
    public: 
        Symbol(string name, int val);
        Symbol(string name, double val);
        Symbol(string name, string val);
        void        print(ostream &os);
		string      get_id() {return m_id;}
		Gpl_type    get_type() {return m_type;}
        int         get_int_val() {return m_int_val;}
        double      get_double_val() {return m_double_val;}
        string      get_string_val() {return m_string_val;}

    private:
        string m_id;
        int m_int_val;
        double m_double_val;
        string m_string_val;
        Gpl_type m_type;

};

#endif
