#ifndef SYMBOL_H
#define SYMBOL_H

#include <iostream>
#include <string>
#include <fstream>
#include <cassert>
#include "gpl_type.h"
#include "game_object.h"
#include "animation_block.h"
#include "indent.h"

using namespace std;

class Symbol
{
    public: 
        Symbol(string name, int val);
        Symbol(string name, double val);
        Symbol(string name, string val);
        Symbol(string name, Game_object *game_object);
        Symbol(string name, Animation_block *animation_block);

        void                print(ostream &os);
		string              get_id() {return m_id;}
		Gpl_type            get_type() {return m_type;}
        
        int                 get_int_val() {return m_int_val;}
        double              get_double_val() {return m_double_val;}
        string              get_string_val() {return m_string_val;}
        Game_object*        get_game_object_val() {return m_game_object;}
        Animation_block*    get_animation_block() {return m_animation_block;}

        void                set_int_val(int value) {m_int_val = value;}
        void                set_double_val(double value) {m_double_val = value;}
        void                set_string_val(string value) {m_string_val = value;}

    private:
        string          m_id;
        int             m_int_val;
        double          m_double_val;
        string          m_string_val;
        Game_object    *m_game_object;
        Gpl_type        m_type;
        Animation_block *m_animation_block;


};

#endif
