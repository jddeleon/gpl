#include "symbol_table.h"

Symbol_table *Symbol_table::m_instance = 0;

Symbol_table *Symbol_table::instance()
{
    if (m_instance == 0)
    {
        m_instance = new Symbol_table();
    }
    return m_instance;
}

void Symbol_table::print(ostream &os)
{
    for (m_iter = m_sym_map.begin(); m_iter != m_sym_map.end(); m_iter++)
    {
        m_iter->second->print(os);
    }
}

bool Symbol_table::insert(string name, Symbol *symbol)
{
    if (this->lookup(name) != NULL)
    {
        return false;
    }
    m_sym_map[name] = symbol;
    return true;
}

Symbol* Symbol_table::lookup(string name)
{
   
    if (m_sym_map.find(name) == m_sym_map.end() && m_sym_map.find(name+"[0]") == m_sym_map.end())
    {
        return NULL;
    }
    m_iter = m_sym_map.find(name);
    return m_iter->second;
}
