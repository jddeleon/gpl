#ifndef SYMBOL_H
#define SYMBOL_H

#include <iostream>
#include <string>
#include <fstream>

using namespace std;
#include "gpl_type.h"

class Symbol
{
    public: 
        Symbol(string name, int val);
        Symbol(string name, double val);
        Symbol(string name, string val);
        void print(ostream &os);

    private:
        string m_id;
        int m_int_val;
        double m_double_val;
        string m_string_val;
        Gpl_type m_type;

};

#endif
