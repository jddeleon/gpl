#include "exit_statement.h"
#include "gpl_type.h"

ExitStmt::ExitStmt(Expr *expr, int line_no)
{
    assert(expr->get_type() == INT);
    m_expr = expr;
    m_line_no = line_no;
}

void ExitStmt::execute()
{
    int val = m_expr->eval_int();

    cout << "gpl[" << m_line_no << "]: exit(" << val << ")" << endl;
    exit(val);
 
}
