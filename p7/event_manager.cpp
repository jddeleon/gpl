#include "event_manager.h"
#include "gpl_assert.h"
using namespace std;

/* static */ Event_manager *Event_manager::m_instance = 0;

/* static */ Event_manager * Event_manager::instance()
{
  if (!m_instance)
    m_instance = new Event_manager();
  return m_instance;
}

Event_manager::Event_manager()
{
}

Event_manager::~Event_manager()
{
}


void 
Event_manager::execute_handlers(Window::Keystroke keystroke)
{
    // execute all the statement blocks for the given keystroke
    vector<Statement_block*> blocks_to_execute = m_stmt_block_vector[keystroke];
    int size = blocks_to_execute.size();

    for (int i = 0; i < size; i++)
        blocks_to_execute[i]->execute();
}

bool Event_manager::register_event(int key, Statement_block* stmt_block)
{
    if (key < 0 || key > Window::NUMBER_OF_KEYS)
        return false;

    m_stmt_block_vector[key].push_back(stmt_block);
    return true;

}
