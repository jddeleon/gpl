#include "variable.h"

// Constructor for SIMPLE variable
Variable::Variable(Symbol* symbol)
{
    assert(symbol != NULL);
    m_symbol = symbol;
    m_id = symbol->get_id();
    m_type = symbol->get_type();
    m_expr = NULL;
    m_kind = SIMPLE;

    if (m_type == INT)
    {
        m_int_val = m_symbol->get_int_val();
    }
    else if (m_type == DOUBLE)
    {
        m_double_val = m_symbol->get_double_val();
    }
    else if (m_type == STRING)
    {
        m_string_val = m_symbol->get_string_val();
    }
    else
    {
        assert(m_type == ANIMATION_BLOCK);
        m_animation_block = m_symbol->get_animation_block();
    }
}

// Constructor for ARRAY variable
Variable::Variable(string id, Expr* expr, Gpl_type type)
{
    assert(expr != NULL);
    m_expr = expr;
    m_type = type;
    m_symbol = NULL;
    m_id = id;
    m_kind = ARRAY;
    
}

// Constructor for MEMBER
Variable::Variable(Game_object* object, string id, string member)
{
    assert(object != NULL);
    m_id = id;
    Status object_status;
    m_kind = MEMBER;
    m_member_id = member;
    
    object_status = object->get_member_variable_type(member, m_type);
    assert(object_status == OK);

    if (m_type == INT)
        object_status = object->get_member_variable(member, m_int_val);
    else if (m_type == DOUBLE)
        object_status = object->get_member_variable(member, m_double_val);
    else
    {
        assert(m_type == STRING);
        object_status = object->get_member_variable(member, m_string_val);
    }

    assert(object_status == OK);


}
// Constructor for MEMBER_ARRAY
Variable::Variable(Game_object* object, string id, Expr* expr, string member)
{
    assert(object != NULL);
    m_id = id;
    Status object_status;
    m_kind = MEMBER_ARRAY;
    m_member_id = member;
    m_expr = expr;
    
    object_status = object->get_member_variable_type(member, m_type);
    assert(object_status == OK);

    if (m_type == INT)
        object_status = object->get_member_variable(member, m_int_val);
    else if (m_type == DOUBLE)
        object_status = object->get_member_variable(member, m_double_val);
    else
    {
        assert(m_type == STRING);
        object_status = object->get_member_variable(member, m_string_val);
    }

    assert(object_status == OK);

}

Symbol* Variable::get_symbol()
{
    Symbol* symbol = NULL;

    if (m_kind == SIMPLE)
    {
        symbol = m_symbol;
    }
    else if (m_kind == ARRAY)
    {
        /* need to evaluate the expression to get the correct index and 
         * then lookup the symbol in the symbol table */
         int index = m_expr->eval_int();
         string id;
        ostringstream s;
        s << m_id << "[" << index << "]";
        id = s.str();

        symbol = Symbol_table::instance()->lookup(id);

    }
    else if (m_kind == MEMBER)
    {
        symbol = Symbol_table::instance()->lookup(m_id);
          
    }
    else if (m_kind == MEMBER_ARRAY)
    {
        int index = m_expr->eval_int();
        string id;
        ostringstream s;
        s << m_id << "[" << index << "]";
        id = s.str();
        
        symbol = Symbol_table::instance()->lookup(id);
    }

    return symbol;
}

int Variable::get_int_val()
{
    Symbol* sym = this->get_symbol();
    assert(sym != NULL);
    
    if (m_kind == SIMPLE || m_kind == ARRAY)
    {
        m_int_val = sym->get_int_val();
    }
    else if (m_kind == MEMBER || m_kind == MEMBER_ARRAY)
    {
        Game_object* obj = sym->get_game_object_val();
        obj->get_member_variable(m_member_id, m_int_val);
    }

    return m_int_val;
}
double Variable::get_double_val()
{
    Symbol* sym = this->get_symbol();
    assert(sym != NULL);

    if (m_kind == SIMPLE || m_kind == ARRAY)
    {
        m_double_val = sym->get_double_val();
    }
    else if (m_kind == MEMBER || m_kind == MEMBER_ARRAY)
    {
        Game_object* obj = sym->get_game_object_val();
        obj->get_member_variable(m_member_id, m_double_val);
    }

    return m_double_val;
    
}
string Variable::get_string_val()
{
    Symbol* sym = this->get_symbol();
    assert(sym != NULL);

    if (m_kind == SIMPLE || m_kind == ARRAY)
    {
        m_string_val = sym->get_string_val();
    }
    else if (m_kind == MEMBER || m_kind == MEMBER_ARRAY)
    {
        Game_object* obj = sym->get_game_object_val();
        obj->get_member_variable(m_member_id, m_string_val);
    }
    return m_string_val;
}
