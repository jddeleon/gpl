/*
  This file contains the input to the bison compiler generator.
  bison will use this file to generate a C/C++ parser.

  The default output file for bison is: y.tab.c

    $ bison gpl.y    will produce the file y.tab.c
*/

%{  // bison syntax to indicate the start of the header
    // the header is copied directly into y.tab.c

extern int yylex();         // this lexer function returns next token
extern int yyerror(char *); // used to print errors
extern int line_count;      // the current line in the input; from array.l

#include "error.h"      // class for printing errors (used by gpl)
#include "gpl_assert.h" // function version of standard assert.h
#include "parser.h"
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <cassert>
#include <stack>
#include "symbol_table.h"
#include "expression.h"
#include "gpl_type.h"
#include "game_object.h"
#include "triangle.h"
#include "circle.h"
#include "rectangle.h"
#include "pixmap.h"
#include "textbox.h"
#include "statement.h"
#include "print_statement.h"
#include "if_statement.h"
#include "event_manager.h"
#include "exit_statement.h"
#include "assignment_statement.h"
#include "for_statement.h"
#include "statement_block.h"

//using namespace std;

// use this global variable to store all the values in the array
// add vectors here for additional types
vector<int> *int_vector;
vector<double> *double_vector;
vector<string*> *string_vector;
Symbol_table *symbol_table = Symbol_table::instance();
Event_manager* event_manager = Event_manager::instance();

// global variable to create a new object being constructed
Game_object* cur_object_under_construction;
string cur_object_id;
stack<Statement_block*> statement_block_stack;

// bison syntax to indicate the end of the header
%} 

// The union is used to declare the variable yylval which is used to
// pass data between the flex generated lexer and the bison generated parser, 
// and to pass values up/down the parse tree.
// 
// A union is kind of like a structure or class, but you can only use one 
// field at a time.  Each line describes one item in the union.  The left hand
// side is the type, the right hand side it out name for the type (the "union_"
// is my convention to indicate that this is a member of the union).
// 
// Unions do not have any error checking.  For example, if you put an int in
// the following union  (my_union.union_int = 42)  and then attempt to use it
// as a string (cout << my_union.union_string) you will get garbage.

// The "%union" is bison syntax
// The "union_" is my convention to indicate a member of the union 
//     (it can be hard to tell what is a union field and what is not)

%union {
 int                union_int;
 std::string       *union_string;  // MUST be a pointer to a string (this sucks!)
 double		        union_double;
 Gpl_type           union_gpl_type;
 Variable          *union_var;
 Expr              *union_expr;
 Operator_type      union_op_type;
 Symbol            *union_symbol;
 Animation_block   *union_anim_block;
 Statement_block   *union_stmt_block;
}

// tokens
%left T_OR                                          // level 14 precedence
%left T_AND                                         // level 13 precedence
%left T_EQUAL T_NOT_EQUAL                           // level 9 precedence
%left T_LESS T_GREATER T_LESS_EQUAL T_GREATER_EQUAL // level 8 precedence
%left T_PLUS T_MINUS                                // level 6 precedence
%left T_ASTERISK T_DIVIDE T_MOD                     // level 5 precedence
%right T_NOT                                        // level 3 precedence

%token T_INT
%token T_DOUBLE
%token T_STRING
%token T_TRIANGLE
%token T_PIXMAP
%token T_CIRCLE
%token T_RECTANGLE
%token T_TEXTBOX
%token T_FORWARD
%token T_INITIALIZATION

%token T_TRUE
%token T_FALSE

%token T_ON
%token T_SPACE
%token T_LEFTARROW
%token T_RIGHTARROW
%token T_UPARROW
%token T_DOWNARROW
%token T_LEFTMOUSE_DOWN
%token T_MIDDLEMOUSE_DOWN
%token T_RIGHTMOUSE_DOWN
%token T_LEFTMOUSE_UP
%token T_MIDDLEMOUSE_UP
%token T_RIGHTMOUSE_UP
%token T_MOUSE_MOVE
%token T_MOUSE_DRAG

%token T_F1
%token T_AKEY
%token T_SKEY
%token T_DKEY
%token T_FKEY
%token T_HKEY
%token T_JKEY
%token T_KKEY
%token T_LKEY
%token T_WKEY

%token T_TOUCHES
%token T_NEAR

%token T_ANIMATION

%token T_IF
%token T_FOR
%nonassoc IF_NO_ELSE
%nonassoc T_ELSE

%token T_EXIT
%token T_PRINT

%token T_LPAREN
%token T_RPAREN
%token T_LBRACE
%token T_RBRACE
%token T_LBRACKET
%token T_RBRACKET
%token T_SEMIC
%token T_COMMA
%token T_PERIOD

%token T_ASSIGN
%token T_PLUS_ASSIGN
%token T_MINUS_ASSIGN
%token T_PLUS_PLUS
%token T_MINUS_MINUS

%token T_SIN
%token T_COS
%token T_TAN
%token T_ASIN
%token T_ACOS
%token T_ATAN
%token T_SQRT
%token T_FLOOR
%token T_ABS
%token T_RANDOM

%nonassoc UNARY_OPS

// the following need a type
// %token <type goes here> TOKEN
%token <union_string> T_ID
%token <union_int> T_INT_CONSTANT
%token <union_double> T_DOUBLE_CONSTANT
%token <union_string> T_STRING_CONSTANT

// special token that does not match any production
// used for characters that are not part of the language
%token T_ERROR

// Grammar symbols that have values associated with them need to be
// declared here.  The above union is used for the "returning" the value.
// 
// NOTE: Values are not really returned as in function calls, but values
// associated with symbols sort of look like functions and the values associated
// with a bison symbol look like return values
//
// Because of the following statement, rules that have the symbol "values" on the 
// right-hand-side can use $n to get the "value" associated with it:
// 
//    declaration: T_ID T_ASSIGN T_LBRACE values T_RBRACE T_SEMIC
// 
// In this example, $6 is the Variable_type associated with the symbol "values" (it is the
// sixth symbol right of the ":").

//%type <union_variable_type> values

%type <union_gpl_type> simple_type
%type <union_var> variable_declaration
%type <union_expr> optional_initializer
%type <union_expr> expression
%type <union_expr> primary_expression
%type <union_op_type> math_operator
%type <union_var> variable
%type <union_int> object_type
%type <union_symbol> animation_parameter
%type <union_anim_block> forward_declaration
%type <union_stmt_block> end_of_statement_block
%type <union_int> keystroke
%type <union_stmt_block> statement_block
%type <union_stmt_block> if_block

%% // indicates the start of the rules
//---------------------------------------------------------------------
program:
    declaration_list block_list
    ;

//---------------------------------------------------------------------
declaration_list:
    declaration_list declaration
    | empty
    ;

//---------------------------------------------------------------------
declaration:
    variable_declaration T_SEMIC
    | object_declaration T_SEMIC
    | forward_declaration T_SEMIC
    ;

//---------------------------------------------------------------------
variable_declaration:
    simple_type  T_ID  optional_initializer
    {   
        // if the symbol exists in the table already, error, else insert it
        // into the table
        if(symbol_table->lookup(*$2) != NULL)
        {
            Error::error(Error::PREVIOUSLY_DECLARED_VARIABLE, *$2);
        }
        else
        {   
            // if optional_initializer == NULL, init to default values
            // else init to expression in optional_initializer

            Symbol *new_symbol = NULL;

            if ($3 == NULL)
            {
                switch($1)
                {
                    case INT:
                        new_symbol = new Symbol(*$2, 0);
                        break;
                    case DOUBLE:
                        new_symbol = new Symbol(*$2, 0.0);
                        break;
                    case STRING:
                        new_symbol = new Symbol(*$2, "");
                        break;
                    default:
                        assert(false);
                }
            }
            else // optional_initializer contains an expression
            {
                switch($1)
                {   
                    case INT:
                        if ($3->get_type() != INT)
                        {   
                            Error::error(Error::INVALID_TYPE_FOR_INITIAL_VALUE, *$2);
                            new_symbol = new Symbol(*$2, 0);
                        }
                        else
                        {
                            int new_int_val = $3->eval_int();
                            new_symbol = new Symbol(*$2, new_int_val);
                        }
                        break;
                    case DOUBLE:
                        if ($3->get_type() == STRING)
                        {   
                            Error::error(Error::INVALID_TYPE_FOR_INITIAL_VALUE, *$2);
                            new_symbol = new Symbol(*$2, 0.0);
                        }
                        else
                        {
                            if ($3->get_type() == INT)
                            {
                                double new_double_val = $3->eval_int();
                                new_symbol = new Symbol(*$2, new_double_val);
                            }
                            else
                            {
                                double new_double_val = $3->eval_double();
                                new_symbol = new Symbol(*$2, new_double_val);
                            }
                        }
                        break;
                    case STRING:
                        if ($3->get_type() == INT)
                        {
                            int int_val = $3->eval_int();
                            ostringstream os_stream;
                            os_stream << int_val;
                            string new_string_val = os_stream.str();
                            new_symbol = new Symbol(*$2, new_string_val);
                        }
                        else if ($3->get_type() == DOUBLE)
                        {
                            double double_val = $3->eval_double();
                            ostringstream os_stream;
                            os_stream << double_val;
                            string new_string_val = os_stream.str();
                            new_symbol = new Symbol(*$2, new_string_val);
                        }
                        else
                        {
                            string new_string_val = $3->eval_string();
                            new_symbol = new Symbol(*$2, new_string_val);
                        }
                        break;
                    default:
                        assert(false);
                }
            }
            
            // try to insert new symbol into table
            assert(symbol_table->insert(*$2, new_symbol) != false);
        }
    }
    | simple_type  T_ID  T_LBRACKET expression T_RBRACKET
    {
        if (symbol_table->lookup(*$2) != NULL)
        {
            Error::error(Error::PREVIOUSLY_DECLARED_VARIABLE, *$2);
        }
        else
        {
            int size;
            Symbol *new_symbol = NULL;
            string id;

        
            if ($4->get_type() == STRING)
            {
                Error::error(Error::INVALID_ARRAY_SIZE, *$2, $4->eval_string());
            }
            else if ($4->get_type() == DOUBLE)
            {
                size = $4->eval_double();
                ostringstream s;
                s << size;
                string i = s.str();

                Error::error(Error::INVALID_ARRAY_SIZE, *$2, i);
            }
            else
            {
                assert($4->get_type() == INT);
                size = $4->eval_int();
                if (size == 0)
                {
                    Error::error(Error::INVALID_ARRAY_SIZE, *$2, "0");
                }
            }
        

            for (int i = 0; i < size; i++)
            {  
                id = *$2; 
                ostringstream index;
                index  << i;
                id += '[' + index.str() + ']';
                
                switch ($1)
                {
                    case INT:
                        new_symbol = new Symbol(id, 0);
                        break;

                    case DOUBLE:
                        new_symbol = new Symbol(id, 0.0);
                        break;

                    case STRING:
                        new_symbol = new Symbol(id, "");
                        break;
                    default:
                        assert(false);
                }
                symbol_table->insert(id, new_symbol);
                
            }
        }
    }        
    ;

//---------------------------------------------------------------------
simple_type:
    T_INT
    {
        $$ = INT;
    }
    | T_DOUBLE
    {
        $$ = DOUBLE;
    }
    | T_STRING
    {
        $$ = STRING;
    }
    ;

//---------------------------------------------------------------------
optional_initializer:
    T_ASSIGN expression
    {
        $$ = $2;
    }
    | empty
    {
        $$ = NULL;
    }
    ;

//---------------------------------------------------------------------
object_declaration:
    object_type T_ID
    {
        switch ($1)
        {
            case T_TRIANGLE:
                cur_object_under_construction = new Triangle();
                break;
            case T_PIXMAP:
                cur_object_under_construction = new Pixmap();
                break;
            case T_CIRCLE:
                cur_object_under_construction = new Circle();
                break;
            case T_RECTANGLE:
                cur_object_under_construction = new Rectangle();
                break;
            case T_TEXTBOX:
                cur_object_under_construction = new Textbox();
                break;
            default:
                assert(false);
        }
        
        if (symbol_table->lookup(*$2) != NULL)
        {
            Error::error(Error::PREVIOUSLY_DECLARED_VARIABLE, *$2);
        }
        else
        {
            Symbol *new_symbol = new Symbol(*$2, cur_object_under_construction);

            // try to insert new symbol into table
            assert(symbol_table->insert(*$2, new_symbol) != false);
            
            // save the name of the object being constructed for error checking
            cur_object_id = *$2;

        }

    } T_LPAREN parameter_list_or_empty T_RPAREN
    | object_type T_ID T_LBRACKET expression T_RBRACKET
    {
        if (symbol_table->lookup(*$2) != NULL)
        {
            Error::error(Error::PREVIOUSLY_DECLARED_VARIABLE, *$2);
        }
        else
        {
            int size;
            Symbol *new_symbol = NULL;
            string id;

        
            if ($4->get_type() == STRING)
            {
                Error::error(Error::INVALID_ARRAY_SIZE, *$2, $4->eval_string());
            }
            else if ($4->get_type() == DOUBLE)
            {
                size = $4->eval_double();
                ostringstream s;
                s << size;
                string i = s.str();

                Error::error(Error::INVALID_ARRAY_SIZE, *$2, i);
            }
            else
            {
                assert($4->get_type() == INT);
                size = $4->eval_int();
                if (size == 0)
                {
                    Error::error(Error::INVALID_ARRAY_SIZE, *$2, "0");
                }
            }
        

            for (int i = 0; i < size; i++)
            {  
                id = *$2; 
                ostringstream index;
                index  << i;
                id += '[' + index.str() + ']';
                
                
                /* Not sure why I need a switch statement here, since all, 
                 * objects, regardless of type are created the same way. Just
                 * following Tyson's suggestion. Remove this later, if not
                 * needed */

                switch ($1)
                {
                    case T_TRIANGLE:
                        cur_object_under_construction = new Triangle();
                        new_symbol = new Symbol(id, cur_object_under_construction);
                        break;
                    case T_RECTANGLE:
                        cur_object_under_construction = new Rectangle();
                        new_symbol = new Symbol(id, cur_object_under_construction);
                        break;
                    case T_CIRCLE:
                        cur_object_under_construction = new Circle();
                        new_symbol = new Symbol(id, cur_object_under_construction);
                        break;
                    case T_PIXMAP:
                        cur_object_under_construction = new Pixmap();
                        new_symbol = new Symbol(id, cur_object_under_construction);
                        break;
                    case T_TEXTBOX:
                        cur_object_under_construction = new Textbox();
                        new_symbol = new Symbol(id, cur_object_under_construction);
                        break;
                    default:
                        assert(false);
                       
                }
                assert(symbol_table->insert(id, new_symbol));
                cur_object_id = id;
                
            }
        }
    }        
    ;

//---------------------------------------------------------------------
object_type:
    T_TRIANGLE
    {
        $$ = T_TRIANGLE;
    }
    | T_PIXMAP
    {
        $$ = T_PIXMAP;
    }
    | T_CIRCLE
    {
        $$ = T_CIRCLE;
    }
    | T_RECTANGLE
    {
        $$ = T_RECTANGLE;
    }
    | T_TEXTBOX
    {
        $$ = T_TEXTBOX;
    }
    ;

//---------------------------------------------------------------------
parameter_list_or_empty :
    parameter_list
    | empty
    ;

//---------------------------------------------------------------------
parameter_list :
    parameter_list T_COMMA parameter
    | parameter
    ;

//---------------------------------------------------------------------
parameter:
    T_ID T_ASSIGN expression
    {

        string object_type = cur_object_under_construction->type();
        Gpl_type member_type; 
        Status object_status = cur_object_under_construction->get_member_variable_type(*$1, member_type);
        
        // check status of object
        if (object_status == MEMBER_NOT_DECLARED)
        {
            Error::error(Error::UNKNOWN_CONSTRUCTOR_PARAMETER, object_type, *$1);
        }
        else
        {

            switch($3->get_type())
            {
                case INT:
                    /* If the parameter is string, cast the int to a string first */
                    if (member_type == STRING)
                    {
                        int val = $3->eval_int();
                        ostringstream s;
                        s << val;
                        string string_val = s.str();
                        
                        object_status = cur_object_under_construction->set_member_variable(*$1, string_val);
                    }
                    else if (member_type == DOUBLE)
                    {
                        double double_val = $3->eval_int();
                        object_status = cur_object_under_construction->set_member_variable(*$1, double_val);
                    }
                    else
                    {
                        assert(member_type == INT);
                        object_status = cur_object_under_construction->set_member_variable(*$1, $3->eval_int());
                    }
                    break;
                case DOUBLE:
                    /* If the parameter is text, cast the double to a string first */
                    if (member_type == STRING)
                    {
                        double val = $3->eval_double();
                        ostringstream s;
                        s << val;
                        string string_val = s.str();
                        
                        object_status = cur_object_under_construction->set_member_variable(*$1, string_val);
                    }
                    else
                    {
                        object_status = cur_object_under_construction->set_member_variable(*$1, $3->eval_double());
                    }
                    break;
                case STRING:
                    object_status = cur_object_under_construction->set_member_variable(*$1, $3->eval_string());
                    break;
                case ANIMATION_BLOCK:
                {
                    /* if the expression is an animation block, need to check that the object is same type */
                    Animation_block *animation_block = $3->eval_animation_block();
                    Symbol *tmp = animation_block->get_parameter_symbol();
                    Game_object *parameter_object = tmp->get_game_object_val();
                    string parameter_type = parameter_object->type();

                    if (object_type != parameter_type)
                    {
                        string param_id = animation_block->name();
                        Error::error(Error::TYPE_MISMATCH_BETWEEN_ANIMATION_BLOCK_AND_OBJECT, cur_object_id, param_id);
                    }
                    else
                    {
                        object_status = cur_object_under_construction->set_member_variable(*$1, $3->eval_animation_block());
                    }
                    break;
                }
                default:
                    assert(false);
            }
        }

        if (object_status == MEMBER_NOT_OF_GIVEN_TYPE)
        {
            Error::error(Error::INCORRECT_CONSTRUCTOR_PARAMETER_TYPE, cur_object_id, *$1);
        }
    }
    ;

//---------------------------------------------------------------------
forward_declaration:
    T_FORWARD T_ANIMATION T_ID T_LPAREN animation_parameter T_RPAREN
    {
        if (symbol_table->lookup(*$3) != NULL)
        {
            Error::error(Error::PREVIOUSLY_DECLARED_VARIABLE, *$3);
        }
        else
        {
            // create new animation block and insert into symbol_table

            Animation_block *new_animation_block = new Animation_block(-666, $5, *$3);
            Symbol *new_symbol = new Symbol(*$3, new_animation_block);
            assert(symbol_table->insert(*$3, new_symbol));
        }
    }
    ;

//---------------------------------------------------------------------
block_list:
    block_list block
    | empty
    ;

//---------------------------------------------------------------------
block:
    initialization_block
    | animation_block
    | on_block
    ;

//---------------------------------------------------------------------
initialization_block:
    T_INITIALIZATION statement_block
    ;

//---------------------------------------------------------------------
animation_block:
    T_ANIMATION T_ID T_LPAREN check_animation_parameter T_RPAREN T_LBRACE { } statement_list T_RBRACE end_of_statement_block
    ;

//---------------------------------------------------------------------
animation_parameter:
    object_type T_ID
    {
        if (symbol_table->lookup(*$2) != NULL)
        {
            Error::error(Error::ANIMATION_PARAMETER_NAME_NOT_UNIQUE, *$2);
        }
        else
        {
            switch ($1)
            {
                case T_TRIANGLE:
                    cur_object_under_construction = new Triangle();
                    break;
                case T_PIXMAP:
                    cur_object_under_construction = new Pixmap();
                    break;
                case T_CIRCLE:
                    cur_object_under_construction = new Circle();
                    break;
                case T_RECTANGLE:
                    cur_object_under_construction = new Rectangle();
                    break;
                case T_TEXTBOX:
                    cur_object_under_construction = new Textbox();
                    break;
                default:
                    assert(false);
            }
            
            // create new game object and insert into symbol_table
            // flag the object as a parameter
            
            cur_object_id = *$2;
            Symbol *new_symbol = new Symbol(*$2, cur_object_under_construction);
            assert(symbol_table->insert(*$2, new_symbol));

            cur_object_under_construction->never_animate();
            cur_object_under_construction->never_draw();
            
            $$ = new_symbol;
        }
    }
    ;

//---------------------------------------------------------------------
check_animation_parameter:
    T_TRIANGLE T_ID
    | T_PIXMAP T_ID
    | T_CIRCLE T_ID
    | T_RECTANGLE T_ID
    | T_TEXTBOX T_ID
    ;

//---------------------------------------------------------------------
on_block:
    T_ON keystroke statement_block
    {
        switch ($2)
        {
            case T_SPACE:
                event_manager->register_event(0, $3);
                break;
            case T_LEFTARROW:
                event_manager->register_event(1, $3);
                break;
            case T_RIGHTARROW:
                event_manager->register_event(2, $3);
                break;
            case T_UPARROW:
                event_manager->register_event(3, $3);
                break;
            case T_DOWNARROW:
                event_manager->register_event(4, $3);
                break;
            case T_LEFTMOUSE_DOWN:
                event_manager->register_event(5, $3);
                break;
            case T_MIDDLEMOUSE_DOWN:
                event_manager->register_event(6, $3);
                break;
            case T_RIGHTMOUSE_DOWN:
                event_manager->register_event(7, $3);
                break;
            case T_LEFTMOUSE_UP:
                event_manager->register_event(8, $3);
                break;
            case T_MIDDLEMOUSE_UP:
                event_manager->register_event(9, $3);
                break;
            case T_RIGHTMOUSE_UP:
                event_manager->register_event(10, $3);
                break;
            case T_MOUSE_MOVE:
                event_manager->register_event(11, $3);
                break;
            case T_MOUSE_DRAG:
                event_manager->register_event(12, $3);
                break;
            case T_F1:
                event_manager->register_event(13, $3);
                break;
            case T_AKEY:
                event_manager->register_event(14, $3);
                break;
            case T_SKEY:
                event_manager->register_event(15, $3);
                break;
            case T_DKEY:
                event_manager->register_event(16, $3);
                break;
            case T_FKEY:
                event_manager->register_event(17, $3);
                break;
            case T_HKEY:
                event_manager->register_event(18, $3);
                break;
            case T_JKEY:
                event_manager->register_event(19, $3);
                break;
            case T_KKEY:
                event_manager->register_event(20, $3);
                break;
            case T_LKEY:
                event_manager->register_event(21, $3);
                break;
            case T_WKEY:
                event_manager->register_event(22, $3);
                break;
            default:
                assert(false);
        }
    }
    ;

//---------------------------------------------------------------------
keystroke:
    T_SPACE
    {
        $$ = T_SPACE;
    }
    | T_UPARROW
    {
        $$ = T_UPARROW;
    }
    | T_DOWNARROW
    {
        $$ = T_DOWNARROW;
    }
    | T_LEFTARROW
    {
        $$ = T_LEFTARROW;
    }
    | T_RIGHTARROW
    {
        $$ = T_RIGHTARROW;
    }
    | T_LEFTMOUSE_DOWN
    {
        $$ = T_LEFTMOUSE_DOWN;
    }
    | T_MIDDLEMOUSE_DOWN
    {
        $$ = T_MIDDLEMOUSE_DOWN;
    }
    | T_RIGHTMOUSE_DOWN
    {
        $$ = T_RIGHTMOUSE_DOWN;
    }
    | T_LEFTMOUSE_UP
    {
        $$ = T_LEFTMOUSE_UP;
    }
    | T_MIDDLEMOUSE_UP
    {
        $$ = T_MIDDLEMOUSE_UP;
    }
    | T_RIGHTMOUSE_UP
    {
        $$ = T_RIGHTMOUSE_UP;
    }
    | T_MOUSE_MOVE
    {
        $$ = T_MOUSE_MOVE;
    }
    | T_MOUSE_DRAG
    {
        $$ = T_MOUSE_DRAG;
    }
    | T_AKEY 
    {
        $$ = T_AKEY;
    }
    | T_SKEY 
    {
        $$ = T_SKEY;
    }
    | T_DKEY 
    {
        $$ = T_DKEY;
    }
    | T_FKEY 
    {
        $$ = T_FKEY;
    }
    | T_HKEY 
    {
        $$ = T_HKEY;
    }
    | T_JKEY 
    {
        $$ = T_JKEY;
    }
    | T_KKEY 
    {
        $$ = T_KKEY;
    }
    | T_LKEY 
    {
        $$ = T_LKEY;
    }
    | T_WKEY 
    {
        $$ = T_WKEY;
    }
    | T_F1
    {
        $$ = T_F1;
    }
    ;

//---------------------------------------------------------------------
if_block:
    statement_block_creator statement end_of_statement_block
    {
        $$ = $3;
    }
    | statement_block
    {
        $$ = $1;
    }
    ;

//---------------------------------------------------------------------
statement_block:
    T_LBRACE statement_block_creator statement_list T_RBRACE end_of_statement_block
    {
        $$ = $5;
    }
    ;

//---------------------------------------------------------------------
statement_block_creator:
    {
        Statement_block *new_stmt_blk = new Statement_block(line_count);
        statement_block_stack.push(new_stmt_blk);
    }
    ;

//---------------------------------------------------------------------
end_of_statement_block:
    {
        $$ = statement_block_stack.top();
        statement_block_stack.pop();
    }
    ;

//---------------------------------------------------------------------
statement_list:
    statement_list statement
    | empty
    ;

//---------------------------------------------------------------------
statement:
    if_statement
    | for_statement
    | assign_statement T_SEMIC
    | print_statement T_SEMIC
    | exit_statement T_SEMIC
    ;

//---------------------------------------------------------------------
if_statement:
    T_IF T_LPAREN expression T_RPAREN if_block %prec IF_NO_ELSE
    {
        if ($3->get_type() != INT)
        {
            Error::error(Error::INVALID_TYPE_FOR_IF_STMT_EXPRESSION);
        }
        
        Statement_block *cur_stmt_block = statement_block_stack.top();
        Statement *new_stmt = new IfStmt($3, $5);
        cur_stmt_block->insert(new_stmt);
    }
    | T_IF T_LPAREN expression T_RPAREN if_block T_ELSE if_block
    {
        if ($3->get_type() != INT)
        {
            Error::error(Error::INVALID_TYPE_FOR_IF_STMT_EXPRESSION);
        }
        Statement_block *cur_stmt_block = statement_block_stack.top();
        Statement *new_stmt = new IfStmt($3, $5, $7);
        cur_stmt_block->insert(new_stmt);
    }
    ;

//---------------------------------------------------------------------
for_statement:
    T_FOR T_LPAREN statement_block_creator assign_statement end_of_statement_block T_SEMIC expression T_SEMIC statement_block_creator assign_statement end_of_statement_block T_RPAREN statement_block
    {
        if ($7->get_type() != INT)
        {
            Error::error(Error::INVALID_TYPE_FOR_FOR_STMT_EXPRESSION);
        }

        Statement_block *cur_stmt_block = statement_block_stack.top();
        Statement *new_stmt = new ForStmt($5, $11, $13, $7);
        cur_stmt_block->insert(new_stmt);
    }
    ;

//---------------------------------------------------------------------
print_statement:
    T_PRINT T_LPAREN expression T_RPAREN
    {
       /* if ($3->get_type() != INT || $3->get_type() != DOUBLE || $3->get_type != STRING)
        {
            Error::error(Error::INVALID_TYPE_FOR_PRINT_STMT_EXPRESSION);
        }*/
        
        Statement_block *cur_stmt_block = statement_block_stack.top();
        Statement *new_stmt = new PrintStmt($3, line_count);
        cur_stmt_block->insert(new_stmt);
    }
    ;

//---------------------------------------------------------------------
exit_statement:
    T_EXIT T_LPAREN expression T_RPAREN
    {
        if ($3->get_type() != INT)
        {
            Error::error(Error::EXIT_STATUS_MUST_BE_AN_INTEGER, gpl_type_to_string($3->get_type()));
        }
        else
        {
            Statement_block *cur_stmt_block = statement_block_stack.top();
            Statement *new_stmt = new ExitStmt($3, line_count);
            cur_stmt_block->insert(new_stmt);
        }
    }
    ;

//---------------------------------------------------------------------
assign_statement:
    variable T_ASSIGN expression
    {
        Gpl_type var_type = $1->get_type();
        Gpl_type expr_type = $3->get_type();

        if (var_type == INT && expr_type == DOUBLE)
        {
            Error::error(Error::ASSIGNMENT_TYPE_ERROR, "int", "double");
        }
        if (var_type == INT && expr_type == STRING)
        {
            Error::error(Error::ASSIGNMENT_TYPE_ERROR, "int", "string");
        }
        if (var_type == DOUBLE && expr_type == STRING)
        {
            Error::error(Error::ASSIGNMENT_TYPE_ERROR, "double", "string");
        }
        if (var_type == GAME_OBJECT)
        {
            Error::error(Error::INVALID_LHS_OF_ASSIGNMENT, $1->get_id(), "game_object");
        }
        

        Statement_block *cur_stmt_block = statement_block_stack.top();
        Statement *new_stmt = new AssignmentStmt($1, $3, ASSIGN);
        cur_stmt_block->insert(new_stmt);
        
    }
    | variable T_PLUS_ASSIGN expression
    {
        Gpl_type var_type = $1->get_type();
        Gpl_type expr_type = $3->get_type();
        
        if (var_type == INT && expr_type == DOUBLE)
        {
            Error::error(Error::PLUS_ASSIGNMENT_TYPE_ERROR, "int", "double");
        }
        if (var_type == INT && expr_type == STRING)
        {
            Error::error(Error::PLUS_ASSIGNMENT_TYPE_ERROR, "int", "string");
        }
        if (var_type == DOUBLE && expr_type == STRING)
        {
            Error::error(Error::PLUS_ASSIGNMENT_TYPE_ERROR, "double", "string");
        }
        if (var_type == GAME_OBJECT)
        {
            Error::error(Error::INVALID_LHS_OF_PLUS_ASSIGNMENT, $1->get_id(), "game_object");
        }

        Statement_block *cur_stmt_block = statement_block_stack.top();
        Statement *new_stmt = new AssignmentStmt($1, $3, PLUS_ASSIGN);
        cur_stmt_block->insert(new_stmt);
        
    }
    | variable T_MINUS_ASSIGN expression
    {
        Gpl_type var_type = $1->get_type();
        Gpl_type expr_type = $3->get_type();
        
        if (var_type == INT && expr_type == DOUBLE)
        {
            Error::error(Error::MINUS_ASSIGNMENT_TYPE_ERROR, "int", "double");
        }
        if (var_type == INT && expr_type == STRING)
        {
            Error::error(Error::MINUS_ASSIGNMENT_TYPE_ERROR, "int", "string");
        }
        if (var_type == DOUBLE && expr_type == STRING)
        {
            Error::error(Error::MINUS_ASSIGNMENT_TYPE_ERROR, "double", "string");
        }
        if (var_type == STRING)
        {
            Error::error(Error::INVALID_LHS_OF_MINUS_ASSIGNMENT, $1->get_id(), gpl_type_to_string(var_type));
        }

        Statement_block *cur_stmt_block = statement_block_stack.top();
        Statement *new_stmt = new AssignmentStmt($1, $3, MINUS_ASSIGN);
        cur_stmt_block->insert(new_stmt);
        
    }
    ;

//---------------------------------------------------------------------
variable:
    T_ID
    {
        // T_ID should already be in symbol table, if not,
        // create temp symbol
        if(symbol_table->lookup(*$1) == NULL)
        {
            Error::error(Error::UNDECLARED_VARIABLE, *$1);
            Symbol *tmp_symbol = new Symbol(*$1, 0);  
            $$ = new Variable(tmp_symbol);
        }
        else
        {
            Symbol *new_symbol =  symbol_table->lookup(*$1); 
            $$ = new Variable(new_symbol);
        }
    }
    | T_ID T_LBRACKET expression T_RBRACKET
    {   
        if ($3->get_type() == DOUBLE)
        {
            Error::error(Error::ARRAY_INDEX_MUST_BE_AN_INTEGER, *$1, "A double expression");
            Expr* new_expr = new Expr(0);
            $$ = new Variable(*$1, new_expr, INT);
        }
        else if ($3->get_type() == STRING)
        {
            Error::error(Error::ARRAY_INDEX_MUST_BE_AN_INTEGER, *$1, "A string expression");
            Expr* new_expr = new Expr(0);
            $$ = new Variable(*$1, new_expr, INT);
        }
        else
        {   
            int index = $3->eval_int();
            ostringstream s;
            s << *$1 << "[0]";
            string id = s.str();

            Symbol* tmp = symbol_table->lookup(id);
                
            $$ = new Variable(*$1, $3, tmp->get_type());
            
        }

    }
    | T_ID T_PERIOD T_ID
    {
        Status object_status;
        Gpl_type object_type;
        Symbol *object_symbol = symbol_table->lookup(*$1);
        Game_object *object;
        
        if (object_symbol == NULL)
        {
            Error::error(Error::UNDECLARED_VARIABLE, *$1);
            Symbol *tmp_symbol = new Symbol(*$1, 0);  
            $$ = new Variable(tmp_symbol);
        }
        else if (object_symbol->get_type() != GAME_OBJECT)
        {
            Error::error(Error::LHS_OF_PERIOD_MUST_BE_OBJECT, *$1);
            Symbol *tmp_symbol = new Symbol(*$1, 0);  
            $$ = new Variable(tmp_symbol);
        }
        else
        {
            object = object_symbol->get_game_object_val();
            object_status = object->get_member_variable_type(*$3, object_type);

            if (object_status == MEMBER_NOT_DECLARED)
            {
                Error::error(Error::UNDECLARED_MEMBER, *$1, *$3);
                Symbol *tmp_symbol = new Symbol(*$1, 0);  
                $$ = new Variable(tmp_symbol);
            }
            else
            {
                assert(object_status == OK);
                $$ = new Variable(object, *$1, *$3);
            }
        }
        
    }
    | T_ID T_LBRACKET expression T_RBRACKET T_PERIOD T_ID
    {
        Status object_status;
        Gpl_type object_type;
        Game_object *object;
        Symbol *object_symbol;
        
        if ($3->get_type() == DOUBLE)
        {
            Error::error(Error::ARRAY_INDEX_MUST_BE_AN_INTEGER, *$1, "A double expression");
            Symbol *tmp_symbol = new Symbol(*$1, 0);  
            $$ = new Variable(tmp_symbol);
        }
        else if ($3->get_type() == STRING)
        {
            Error::error(Error::ARRAY_INDEX_MUST_BE_AN_INTEGER, *$1, "A string expression");
            Symbol *tmp_symbol = new Symbol(*$1, 0);  
            $$ = new Variable(tmp_symbol); 
        }
        else // type is INT
        {
            int index = $3->eval_int();
            ostringstream s;
            s << *$1 << "[" << index << "]";
            string id = s.str();
            
            object_symbol = symbol_table->lookup(id);

            if (symbol_table->lookup(*$1) == NULL)
            {
                Error::error(Error::UNDECLARED_VARIABLE, *$1);
                Symbol *tmp_symbol = new Symbol(*$1, 0);  
                $$ = new Variable(tmp_symbol);
            }
            else if (object_symbol->get_type() != GAME_OBJECT)
            {
                Error::error(Error::LHS_OF_PERIOD_MUST_BE_OBJECT, *$1);
                Symbol *tmp_symbol = new Symbol(*$1, 0);  
                $$ = new Variable(tmp_symbol);
            }
            else
            {
                object = object_symbol->get_game_object_val();
                object_status = object->get_member_variable_type(*$6, object_type);
            
            
                if (object_status == MEMBER_NOT_DECLARED)
                {
                    Error::error(Error::UNDECLARED_MEMBER, *$1, *$6);
                    Symbol *tmp_symbol = new Symbol(*$1, 0);  
                    $$ = new Variable(tmp_symbol);
                }
                else if (object_symbol == NULL)
                {
                    ostringstream s;
                    s << index;
                    string i = s.str();

                    Error::error(Error::ARRAY_INDEX_OUT_OF_BOUNDS, *$1, i);
                    Symbol *tmp_symbol = new Symbol(*$1, 0);  
                    $$ = new Variable(tmp_symbol);
                }
                else
                {
                    assert(object_status == OK);
                    $$ = new Variable(object, *$1, $3, *$6);
                }        
            }
        }
    }
    ;

//---------------------------------------------------------------------
expression:
    primary_expression
    {
        $$ = $1;
    }
    | expression T_OR expression
    {   
        if($1->get_type() == STRING)
        {
            Error::error(Error::INVALID_LEFT_OPERAND_TYPE, "||");
            $$ = new Expr(0);
        }
        else if ($3->get_type() == STRING)
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "||");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($1, $3, OR);
        }
    }
    | expression T_AND expression
    {
        if ($1->get_type() == STRING)
        { 
            Error::error(Error::INVALID_LEFT_OPERAND_TYPE, "&&");
            $$ = new Expr(0);
        }
        else if ($3->get_type() == STRING)
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "&&");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($1, $3, AND);
        }
    }
    | expression T_LESS_EQUAL expression
    {
        $$ = new Expr($1, $3, LESS_THAN_EQUAL);
    }
    | expression T_GREATER_EQUAL  expression
    {
        $$ = new Expr($1, $3, GREATER_THAN_EQUAL);
    }
    | expression T_LESS expression 
    {
        $$ = new Expr($1, $3, LESS_THAN);
    }
    | expression T_GREATER  expression
    {
        $$ = new Expr($1, $3, GREATER_THAN);
    }
    | expression T_EQUAL expression
    {
        $$ = new Expr($1, $3, EQUAL);
    }
    | expression T_NOT_EQUAL expression
    {
        $$ = new Expr($1, $3, NOT_EQUAL);
    }
    | expression T_PLUS expression 
    {   
        $$ = new Expr($1, $3, PLUS);
    }
    | expression T_MINUS expression
    {
        if ($1->get_type() == STRING)
        {
            Error::error(Error::INVALID_LEFT_OPERAND_TYPE, "-");
            $$ = new Expr(0);
        }
        else if ($3->get_type() == STRING)
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "-");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($1, $3, MINUS);
        }
    }
    | expression T_ASTERISK expression
    {
        if ($1->get_type() == STRING)
        {
            Error::error(Error::INVALID_LEFT_OPERAND_TYPE, "*");
            $$ = new Expr(0);
        }
        else if ($3->get_type() == STRING)
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "*");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($1, $3, MULTIPLY);
        }
    }
    | expression T_DIVIDE expression
    {
        if ($1->get_type() == STRING)
        {
            Error::error(Error::INVALID_LEFT_OPERAND_TYPE, "/");
            $$ = new Expr(0);
        }
        else if ($3->get_type() == STRING)
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "/");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($1, $3, DIVIDE);
        }
    }
    | expression T_MOD expression
    {
        if (($1->get_type() == STRING) || ($1->get_type() == DOUBLE))
        {
            Error::error(Error::INVALID_LEFT_OPERAND_TYPE, "%");
            $$ = new Expr(0);
        }
        else if (($3->get_type() == STRING) || ($3->get_type() == DOUBLE))
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "%");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($1, $3, MOD);
        }
    }
    | T_MINUS  expression %prec UNARY_OPS
    {
        if ($2->get_type() == STRING)
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "-");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($2, UNARY_MINUS);
        }
    }
    | T_NOT  expression %prec UNARY_OPS
    {
        if ($2->get_type() == STRING)
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "!");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($2, NOT);
        }
    }
    | math_operator T_LPAREN expression T_RPAREN
    {
        if ($3->get_type() == STRING)
        {   
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, operator_to_string($1));
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($3, $1);
        }
    }
    | variable geometric_operator variable
    {
    }
    ;

//---------------------------------------------------------------------
primary_expression:
    T_LPAREN  expression T_RPAREN
    {
        $$ = $2;
    }
    | variable
    {
        $$ = new Expr($1);
    }
    | T_INT_CONSTANT
    {
        $$ = new Expr($1);
    }
    | T_TRUE
    {
        $$ = new Expr(1);
    }
    | T_FALSE
    {
        $$ = new Expr(0);
    }
    | T_DOUBLE_CONSTANT
    {
        $$ = new Expr($1);
    }
    | T_STRING_CONSTANT
    {
        string string_const = *$1;
        $$ = new Expr(string_const);
    }
    ;

//---------------------------------------------------------------------
geometric_operator:
    T_TOUCHES
    | T_NEAR
    ;

//---------------------------------------------------------------------
math_operator:
    T_SIN
    {
        $$ = SIN;
    }
    | T_COS
    {
        $$ = COS;
    }
    | T_TAN
    {
        $$ = TAN;
    }
    | T_ASIN
    {
        $$ = ASIN;
    }
    | T_ACOS
    {
        $$ = ACOS;
    }
    | T_ATAN
    {
        $$ = ATAN;
    }
    | T_SQRT
    {
        $$ = SQRT;
    }
    | T_ABS
    {
        $$ = ABS;
    }
    | T_FLOOR
    {
        $$ = FLOOR;
    }
    | T_RANDOM
    {
        $$ = RANDOM;
    }
    ;

//---------------------------------------------------------------------
empty:
    // empty goes to nothing so that you can use empty in productions
    // when you want a production to go to nothing
    ;
