#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

#include "symbol.h"
#include <iostream>
#include <string>
#include <map>
#include "error.h"

using namespace std;

class Symbol_table
{
    public:
        static Symbol_table *instance();
        void print(ostream &os);
        bool insert(string name, Symbol *symbol);
        Symbol* lookup(string name); // should return Symbol* instead of bool
    private:
        static Symbol_table *m_instance;
        Symbol_table() {};
        
        // disable default copy constructor and default assignment
        Symbol_table(const Symbol_table &);
        const Symbol_table &operator=(const Symbol_table &);

        map<string, Symbol*> m_sym_map;
        map<string, Symbol*>::iterator m_iter;

};

#endif
