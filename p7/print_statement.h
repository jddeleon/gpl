#ifndef PRINT_STATEMENT_H
#define PRINT_STATEMENT_H

#include "statement.h"
#include "expression.h"

class PrintStmt : public Statement
{
    public:
        PrintStmt(Expr *expr, int line_no);

        virtual void execute();

    private:
        Expr *m_expr;
        int   m_line_no;

};

#endif
