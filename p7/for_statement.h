#ifndef FOR_STATEMENT_H
#define FOR_STATEMENT_H

#include "statement.h"
#include "statement_block.h"
#include "expression.h"

class ForStmt : public Statement
{
    public:
        ForStmt(Statement_block* initializer, Statement_block* incrementor, 
                Statement_block* body, Expr* expr);

        virtual void execute();

    private:
        Statement_block     *m_initializer;
        Statement_block     *m_incrementor;
        Statement_block     *m_body;
        Expr                *m_expr;

};

#endif
