/*
  This file contains the input to the bison compiler generator.
  bison will use this file to generate a C/C++ parser.

  The default output file for bison is: y.tab.c

    $ bison gpl.y    will produce the file y.tab.c
*/

%{  // bison syntax to indicate the start of the header
    // the header is copied directly into y.tab.c

extern int yylex();         // this lexer function returns next token
extern int yyerror(char *); // used to print errors
extern int line_count;      // the current line in the input; from array.l

#include "error.h"      // class for printing errors (used by gpl)
#include "gpl_assert.h" // function version of standard assert.h
#include "parser.h"
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <cassert>
#include "symbol_table.h"
#include "expression.h"
#include "gpl_type.h"

//using namespace std;

// use this global variable to store all the values in the array
// add vectors here for additional types
vector<int> *int_vector;
vector<double> *double_vector;
vector<string*> *string_vector;
Symbol_table *symbol_table = Symbol_table::instance();


// bison syntax to indicate the end of the header
%} 

// The union is used to declare the variable yylval which is used to
// pass data between the flex generated lexer and the bison generated parser, 
// and to pass values up/down the parse tree.
// 
// A union is kind of like a structure or class, but you can only use one 
// field at a time.  Each line describes one item in the union.  The left hand
// side is the type, the right hand side it out name for the type (the "union_"
// is my convention to indicate that this is a member of the union).
// 
// Unions do not have any error checking.  For example, if you put an int in
// the following union  (my_union.union_int = 42)  and then attempt to use it
// as a string (cout << my_union.union_string) you will get garbage.

// The "%union" is bison syntax
// The "union_" is my convention to indicate a member of the union 
//     (it can be hard to tell what is a union field and what is not)

%union {
 int                union_int;
 std::string       *union_string;  // MUST be a pointer to a string (this sucks!)
 double		        union_double;
 Gpl_type           union_gpl_type;
 Variable          *union_var;
 Expr              *union_expr;
 Operator_type      union_op_type;
}

// tokens
%left T_OR                                          // level 14 precedence
%left T_AND                                         // level 13 precedence
%left T_EQUAL T_NOT_EQUAL                           // level 9 precedence
%left T_LESS T_GREATER T_LESS_EQUAL T_GREATER_EQUAL // level 8 precedence
%left T_PLUS T_MINUS                                // level 6 precedence
%left T_ASTERISK T_DIVIDE T_MOD                     // level 5 precedence
%right T_NOT                                        // level 3 precedence

%token T_INT
%token T_DOUBLE
%token T_STRING
%token T_TRIANGLE
%token T_PIXMAP
%token T_CIRCLE
%token T_RECTANGLE
%token T_TEXTBOX
%token T_FORWARD
%token T_INITIALIZATION

%token T_TRUE
%token T_FALSE

%token T_ON
%token T_SPACE
%token T_LEFTARROW
%token T_RIGHTARROW
%token T_UPARROW
%token T_DOWNARROW
%token T_LEFTMOUSE_DOWN
%token T_MIDDLEMOUSE_DOWN
%token T_RIGHTMOUSE_DOWN
%token T_LEFTMOUSE_UP
%token T_MIDDLEMOUSE_UP
%token T_RIGHTMOUSE_UP
%token T_MOUSE_MOVE
%token T_MOUSE_DRAG

%token T_F1
%token T_AKEY
%token T_SKEY
%token T_DKEY
%token T_FKEY
%token T_HKEY
%token T_JKEY
%token T_KKEY
%token T_LKEY
%token T_WKEY

%token T_TOUCHES
%token T_NEAR

%token T_ANIMATION

%token T_IF
%token T_FOR
%nonassoc IF_NO_ELSE
%nonassoc T_ELSE

%token T_EXIT
%token T_PRINT

%token T_LPAREN
%token T_RPAREN
%token T_LBRACE
%token T_RBRACE
%token T_LBRACKET
%token T_RBRACKET
%token T_SEMIC
%token T_COMMA
%token T_PERIOD

%token T_ASSIGN
%token T_PLUS_ASSIGN
%token T_MINUS_ASSIGN
%token T_PLUS_PLUS
%token T_MINUS_MINUS

%token T_SIN
%token T_COS
%token T_TAN
%token T_ASIN
%token T_ACOS
%token T_ATAN
%token T_SQRT
%token T_FLOOR
%token T_ABS
%token T_RANDOM

%nonassoc UNARY_OPS

// the following need a type
// %token <type goes here> TOKEN
%token <union_string> T_ID
%token <union_int> T_INT_CONSTANT
%token <union_double> T_DOUBLE_CONSTANT
%token <union_string> T_STRING_CONSTANT

// special token that does not match any production
// used for characters that are not part of the language
%token T_ERROR

// Grammar symbols that have values associated with them need to be
// declared here.  The above union is used for the "returning" the value.
// 
// NOTE: Values are not really returned as in function calls, but values
// associated with symbols sort of look like functions and the values associated
// with a bison symbol look like return values
//
// Because of the following statement, rules that have the symbol "values" on the 
// right-hand-side can use $n to get the "value" associated with it:
// 
//    declaration: T_ID T_ASSIGN T_LBRACE values T_RBRACE T_SEMIC
// 
// In this example, $6 is the Variable_type associated with the symbol "values" (it is the
// sixth symbol right of the ":").

//%type <union_variable_type> values

%type <union_gpl_type> simple_type
%type <union_var> variable_declaration
%type <union_expr> optional_initializer
%type <union_expr> expression
%type <union_expr> primary_expression
%type <union_op_type> math_operator
%type <union_var> variable

%% // indicates the start of the rules
//---------------------------------------------------------------------
program:
    declaration_list block_list
    ;

//---------------------------------------------------------------------
declaration_list:
    declaration_list declaration
    | empty
    ;

//---------------------------------------------------------------------
declaration:
    variable_declaration T_SEMIC
    | object_declaration T_SEMIC
    | forward_declaration T_SEMIC
    ;

//---------------------------------------------------------------------
variable_declaration:
    simple_type  T_ID  optional_initializer
    {   
        // if the symbol exists in the table already, error, else insert it
        // into the table
        if(symbol_table->lookup(*$2) != NULL)
        {
            Error::error(Error::PREVIOUSLY_DECLARED_VARIABLE, *$2);
        }
        else
        {   
            // if optional_initializer == NULL, init to default values
            // else init to expression in optional_initializer

            Symbol *new_symbol = NULL;

            if ($3 == NULL)
            {
                switch($1)
                {
                    case INT:
                        new_symbol = new Symbol(*$2, 0);
                        break;
                    case DOUBLE:
                        new_symbol = new Symbol(*$2, 0.0);
                        break;
                    case STRING:
                        new_symbol = new Symbol(*$2, "");
                        break;
                }
            }
            else // optional_initializer contains an expression
            {
                switch($1)
                {   
                    case INT:
                        if ($3->get_type() != INT)
                        {   
                            Error::error(Error::INVALID_TYPE_FOR_INITIAL_VALUE, *$2);
                            new_symbol = new Symbol(*$2, 0);
                        }
                        else
                        {
                            int new_int_val = $3->eval_int();
                            new_symbol = new Symbol(*$2, new_int_val);
                        }
                        break;
                    case DOUBLE:
                        if ($3->get_type() == STRING)
                        {   
                            Error::error(Error::INVALID_TYPE_FOR_INITIAL_VALUE, *$2);
                            new_symbol = new Symbol(*$2, 0.0);
                        }
                        else
                        {
                            if ($3->get_type() == INT)
                            {
                                double new_double_val = $3->eval_int();
                                new_symbol = new Symbol(*$2, new_double_val);
                            }
                            else
                            {
                                double new_double_val = $3->eval_double();
                                new_symbol = new Symbol(*$2, new_double_val);
                            }
                        }
                        break;
                    case STRING:
                        if ($3->get_type() == INT)
                        {
                            int int_val = $3->eval_int();
                            ostringstream os_stream;
                            os_stream << int_val;
                            string new_string_val = os_stream.str();
                            new_symbol = new Symbol(*$2, new_string_val);
                        }
                        else if ($3->get_type() == DOUBLE)
                        {
                            double double_val = $3->eval_double();
                            ostringstream os_stream;
                            os_stream << double_val;
                            string new_string_val = os_stream.str();
                            new_symbol = new Symbol(*$2, new_string_val);
                        }
                        else
                        {
                            string new_string_val = $3->eval_string();
                            new_symbol = new Symbol(*$2, new_string_val);
                        }
                        break;
                }
            }
            
            // try to insert new symbol into table
            assert(symbol_table->insert(*$2, new_symbol) != false);
        }
    }
    | simple_type  T_ID  T_LBRACKET expression T_RBRACKET
    {
        if (symbol_table->lookup(*$2) != NULL)
        {
            Error::error(Error::PREVIOUSLY_DECLARED_VARIABLE, *$2);
        }
        else
        {
            int size;
            Symbol *new_symbol = NULL;
            string id;

        
            if ($4->get_type() == STRING)
            {
                Error::error(Error::INVALID_ARRAY_SIZE, *$2, $4->eval_string());
            }
            else if ($4->get_type() == DOUBLE)
            {
                double d_size = $4->eval_double();
                ostringstream s;
                s << d_size;
                string i = s.str();

                Error::error(Error::INVALID_ARRAY_SIZE, *$2, i);
            }
            else
            {
                assert($4->get_type() == INT);
                size = $4->eval_int();
                if (size == 0)
                {
                    Error::error(Error::INVALID_ARRAY_SIZE, *$2, "0");
                }
            }
        

            for (int i = 0; i < size; i++)
            {  
                id = *$2; 
                ostringstream index;
                index  << i;
                id += '[' + index.str() + ']';
                
                switch ($1)
                {
                    case INT:
                        new_symbol = new Symbol(id, 0);
                        break;

                    case DOUBLE:
                        new_symbol = new Symbol(id, 0.0);
                        break;

                    case STRING:
                        new_symbol = new Symbol(id, "");
                        break;                        
                }
                symbol_table->insert(id, new_symbol);
                
            }
        }
    }        
    ;

//---------------------------------------------------------------------
simple_type:
    T_INT
    {
        $$ = INT;
    }
    | T_DOUBLE
    {
        $$ = DOUBLE;
    }
    | T_STRING
    {
        $$ = STRING;
    }
    ;

//---------------------------------------------------------------------
optional_initializer:
    T_ASSIGN expression
    {
        $$ = $2;
    }
    | empty
    {
        $$ = NULL;
    }
    ;

//---------------------------------------------------------------------
object_declaration:
    object_type T_ID T_LPAREN parameter_list_or_empty T_RPAREN
    | object_type T_ID T_LBRACKET expression T_RBRACKET
    ;

//---------------------------------------------------------------------
object_type:
    T_TRIANGLE
    | T_PIXMAP
    | T_CIRCLE
    | T_RECTANGLE
    | T_TEXTBOX
    ;

//---------------------------------------------------------------------
parameter_list_or_empty :
    parameter_list
    | empty
    ;

//---------------------------------------------------------------------
parameter_list :
    parameter_list T_COMMA parameter
    | parameter
    ;

//---------------------------------------------------------------------
parameter:
    T_ID T_ASSIGN expression
    ;

//---------------------------------------------------------------------
forward_declaration:
    T_FORWARD T_ANIMATION T_ID T_LPAREN animation_parameter T_RPAREN
    ;

//---------------------------------------------------------------------
block_list:
    block_list block
    | empty
    ;

//---------------------------------------------------------------------
block:
    initialization_block
    | animation_block
    | on_block
    ;

//---------------------------------------------------------------------
initialization_block:
    T_INITIALIZATION statement_block
    ;

//---------------------------------------------------------------------
animation_block:
    T_ANIMATION T_ID T_LPAREN check_animation_parameter T_RPAREN T_LBRACE { } statement_list T_RBRACE end_of_statement_block
    ;

//---------------------------------------------------------------------
animation_parameter:
    object_type T_ID
    ;

//---------------------------------------------------------------------
check_animation_parameter:
    T_TRIANGLE T_ID
    | T_PIXMAP T_ID
    | T_CIRCLE T_ID
    | T_RECTANGLE T_ID
    | T_TEXTBOX T_ID
    ;

//---------------------------------------------------------------------
on_block:
    T_ON keystroke statement_block
    ;

//---------------------------------------------------------------------
keystroke:
    T_SPACE
    | T_UPARROW
    | T_DOWNARROW
    | T_LEFTARROW
    | T_RIGHTARROW
    | T_LEFTMOUSE_DOWN
    | T_MIDDLEMOUSE_DOWN
    | T_RIGHTMOUSE_DOWN
    | T_LEFTMOUSE_UP
    | T_MIDDLEMOUSE_UP
    | T_RIGHTMOUSE_UP
    | T_MOUSE_MOVE
    | T_MOUSE_DRAG
    | T_AKEY 
    | T_SKEY 
    | T_DKEY 
    | T_FKEY 
    | T_HKEY 
    | T_JKEY 
    | T_KKEY 
    | T_LKEY 
    | T_WKEY 
    | T_F1
    ;

//---------------------------------------------------------------------
if_block:
    statement_block_creator statement end_of_statement_block
    | statement_block
    ;

//---------------------------------------------------------------------
statement_block:
    T_LBRACE statement_block_creator statement_list T_RBRACE end_of_statement_block
    ;

//---------------------------------------------------------------------
statement_block_creator:
    // this goes to nothing so that you can put an action here in p7
    ;

//---------------------------------------------------------------------
end_of_statement_block:
    // this goes to nothing so that you can put an action here in p7
    ;

//---------------------------------------------------------------------
statement_list:
    statement_list statement
    | empty
    ;

//---------------------------------------------------------------------
statement:
    if_statement
    | for_statement
    | assign_statement T_SEMIC
    | print_statement T_SEMIC
    | exit_statement T_SEMIC
    ;

//---------------------------------------------------------------------
if_statement:
    T_IF T_LPAREN expression T_RPAREN if_block %prec IF_NO_ELSE
    | T_IF T_LPAREN expression T_RPAREN if_block T_ELSE if_block
    ;

//---------------------------------------------------------------------
for_statement:
    T_FOR T_LPAREN statement_block_creator assign_statement end_of_statement_block T_SEMIC expression T_SEMIC statement_block_creator assign_statement end_of_statement_block T_RPAREN statement_block
    ;

//---------------------------------------------------------------------
print_statement:
    T_PRINT T_LPAREN expression T_RPAREN
    ;

//---------------------------------------------------------------------
exit_statement:
    T_EXIT T_LPAREN expression T_RPAREN
    ;

//---------------------------------------------------------------------
assign_statement:
    variable T_ASSIGN expression
    | variable T_PLUS_ASSIGN expression
    | variable T_MINUS_ASSIGN expression
    ;

//---------------------------------------------------------------------
variable:
    T_ID
    {
        // T_ID should already be in symbol table, if not,
        // create temp symbol
        if(symbol_table->lookup(*$1) == NULL)
        {
            Error::error(Error::UNDECLARED_VARIABLE, *$1);
            Symbol *tmp_symbol = new Symbol(*$1, 0);  
            $$ = new Variable(tmp_symbol);
        }
        else
        {
            Symbol *new_symbol =  symbol_table->lookup(*$1); 
            $$ = new Variable(new_symbol);
        }
    }
    | T_ID T_LBRACKET expression T_RBRACKET
    {   
        if ($3->get_type() == DOUBLE)
        {
            Error::error(Error::ARRAY_INDEX_MUST_BE_AN_INTEGER, *$1, "A double expression");
            Expr* new_expr = new Expr(0);
            $$ = new Variable(new_expr);
        }
        else if ($3->get_type() == STRING)
        {
            Error::error(Error::ARRAY_INDEX_MUST_BE_AN_INTEGER, *$1, "A string expression");
            Expr* new_expr = new Expr(0);
            $$ = new Variable(new_expr);
        }
        else
        {   
            int index = $3->eval_int();
            ostringstream s;
            s << *$1 << "[" << index << "]";
            string id = s.str();

            if (symbol_table->lookup(id) == NULL)
            {   
                ostringstream s;
                s << index;
                string i = s.str();

                Error::error(Error::ARRAY_INDEX_OUT_OF_BOUNDS, *$1, i);
                Expr* new_expr = new Expr(0);
     
                $$ = new Variable(new_expr);
            }
            else
            {
                $$ = new Variable($3);
            }
        }

    }
    | T_ID T_PERIOD T_ID
    {
    }
    | T_ID T_LBRACKET expression T_RBRACKET T_PERIOD T_ID
    {
    }
    ;

//---------------------------------------------------------------------
expression:
    primary_expression
    {
        $$ = $1;
    }
    | expression T_OR expression
    {   
        if($1->get_type() == STRING)
        {
            Error::error(Error::INVALID_LEFT_OPERAND_TYPE, "||");
            $$ = new Expr(0);
        }
        else if ($3->get_type() == STRING)
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "||");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($1, $3, OR);
        }
    }
    | expression T_AND expression
    {
        if ($1->get_type() == STRING)
        { 
            Error::error(Error::INVALID_LEFT_OPERAND_TYPE, "&&");
            $$ = new Expr(0);
        }
        else if ($3->get_type() == STRING)
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "&&");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($1, $3, AND);
        }
    }
    | expression T_LESS_EQUAL expression
    {
        $$ = new Expr($1, $3, LESS_THAN_EQUAL);
    }
    | expression T_GREATER_EQUAL  expression
    {
        $$ = new Expr($1, $3, GREATER_THAN_EQUAL);
    }
    | expression T_LESS expression 
    {
        $$ = new Expr($1, $3, LESS_THAN);
    }
    | expression T_GREATER  expression
    {
        $$ = new Expr($1, $3, GREATER_THAN);
    }
    | expression T_EQUAL expression
    {
        $$ = new Expr($1, $3, EQUAL);
    }
    | expression T_NOT_EQUAL expression
    {
        $$ = new Expr($1, $3, NOT_EQUAL);
    }
    | expression T_PLUS expression 
    {   
        $$ = new Expr($1, $3, PLUS);
    }
    | expression T_MINUS expression
    {
        if ($1->get_type() == STRING)
        {
            Error::error(Error::INVALID_LEFT_OPERAND_TYPE, "-");
            $$ = new Expr(0);
        }
        else if ($3->get_type() == STRING)
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "-");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($1, $3, MINUS);
        }
    }
    | expression T_ASTERISK expression
    {
        if ($1->get_type() == STRING)
        {
            Error::error(Error::INVALID_LEFT_OPERAND_TYPE, "*");
            $$ = new Expr(0);
        }
        else if ($3->get_type() == STRING)
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "*");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($1, $3, MULTIPLY);
        }
    }
    | expression T_DIVIDE expression
    {
        if ($1->get_type() == STRING)
        {
            Error::error(Error::INVALID_LEFT_OPERAND_TYPE, "/");
            $$ = new Expr(0);
        }
        else if ($3->get_type() == STRING)
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "/");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($1, $3, DIVIDE);
        }
    }
    | expression T_MOD expression
    {
        if (($1->get_type() == STRING) || ($1->get_type() == DOUBLE))
        {
            Error::error(Error::INVALID_LEFT_OPERAND_TYPE, "%");
            $$ = new Expr(0);
        }
        else if (($3->get_type() == STRING) || ($3->get_type() == DOUBLE))
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "%");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($1, $3, MOD);
        }
    }
    | T_MINUS  expression %prec UNARY_OPS
    {
        if ($2->get_type() == STRING)
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "-");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($2, UNARY_MINUS);
        }
    }
    | T_NOT  expression %prec UNARY_OPS
    {
        if ($2->get_type() == STRING)
        {
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, "!");
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($2, NOT);
        }
    }
    | math_operator T_LPAREN expression T_RPAREN
    {
        if ($3->get_type() == STRING)
        {   
            string op;

            switch($1)
            {
                case PLUS:
                    op = "+";
                    break;
                case MINUS:
                    op = "-";
                    break;
                case DIVIDE:
                    op = "/";
                    break;
                case MULTIPLY:
                    op = "*";
                    break;
                case MOD:
                    op = "+";
                    break;
                case UNARY_MINUS:
                    op = "-";
                    break;
                case NOT:
                    op = "!";
                    break;
                case AND:
                    op = "&&";
                    break;
                case OR:
                    op = "||";
                    break;
                case EQUAL:
                    op = "==";
                    break;
                case NOT_EQUAL:
                    op = "!=";
                    break;
                case LESS_THAN:
                    op = "<";
                    break;
                case LESS_THAN_EQUAL:
                    op = "<=";
                    break;
                case GREATER_THAN:
                    op = ">";
                    break;
                case GREATER_THAN_EQUAL:
                    op = ">=";
                    break;
                case NEAR:
                    op = "near";
                    break;
                case TOUCHES:
                    op = "touches";
                    break;
                case SIN:
                    op = "sin";
                    break;
                case COS:
                    op = "cos";
                    break;
                case TAN:
                    op = "tan";
                    break;
                case ASIN:
                    op = "asin";
                    break;
                case ACOS:
                    op = "acos";
                    break;
                case ATAN:
                    op = "atan";
                    break;
                case SQRT:
                    op = "sqrt";
                    break;
                case ABS:
                    op = "abs";
                    break;
                case FLOOR:
                    op = "floor";
                    break;
                case RANDOM:
                    op = "random";
                    break;
                default: assert(false);
            }
            Error::error(Error::INVALID_RIGHT_OPERAND_TYPE, op);
            $$ = new Expr(0);
        }
        else
        {
            $$ = new Expr($3, $1);
        }
    }
    | variable geometric_operator variable
    {
    }
    ;

//---------------------------------------------------------------------
primary_expression:
    T_LPAREN  expression T_RPAREN
    {
        $$ = $2;
    }
    | variable
    {
        $$ = new Expr($1);
    }
    | T_INT_CONSTANT
    {
        $$ = new Expr($1);
    }
    | T_TRUE
    {
        $$ = new Expr(1);
    }
    | T_FALSE
    {
        $$ = new Expr(0);
    }
    | T_DOUBLE_CONSTANT
    {
        $$ = new Expr($1);
    }
    | T_STRING_CONSTANT
    {
        string string_const = *$1;
        $$ = new Expr(string_const);
    }
    ;

//---------------------------------------------------------------------
geometric_operator:
    T_TOUCHES
    | T_NEAR
    ;

//---------------------------------------------------------------------
math_operator:
    T_SIN
    {
        $$ = SIN;
    }
    | T_COS
    {
        $$ = COS;
    }
    | T_TAN
    {
        $$ = TAN;
    }
    | T_ASIN
    {
        $$ = ASIN;
    }
    | T_ACOS
    {
        $$ = ACOS;
    }
    | T_ATAN
    {
        $$ = ATAN;
    }
    | T_SQRT
    {
        $$ = SQRT;
    }
    | T_ABS
    {
        $$ = ABS;
    }
    | T_FLOOR
    {
        $$ = FLOOR;
    }
    | T_RANDOM
    {
        $$ = RANDOM;
    }
    ;

//---------------------------------------------------------------------
empty:
    // empty goes to nothing so that you can use empty in productions
    // when you want a production to go to nothing
    ;
