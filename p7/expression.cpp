#include "expression.h"

// Constructor for binary expression
Expr::Expr(Expr* lhs, Expr* rhs, Operator_type op)
{
    m_op = op;
    m_kind = EXPRESSION;
    m_left = lhs;
    m_right = rhs;

    switch (m_op)
    {
        case PLUS:
            if (m_left->get_type() == INT && m_right->get_type() == INT)
            {
                m_type = INT;
            }
            else if ((m_left->get_type() == DOUBLE && m_right->get_type() == INT) ||
                     (m_left->get_type() == INT && m_right->get_type() == DOUBLE) ||
                     (m_left->get_type() == DOUBLE && m_right->get_type() == DOUBLE))
            {
                m_type = DOUBLE;
            }
            else
            {
                m_type = STRING;
            }
            break;

        case MINUS:
        case MULTIPLY:
        case DIVIDE:
            if (m_left->get_type() == INT && m_right->get_type() == INT)
            {
                m_type = INT;
            }
            else
            {
                m_type = DOUBLE;
            }
            break;

        case MOD:
            assert (m_left->get_type() == INT && m_right->get_type() == INT);
            m_type = INT;
            break;
        
        case LESS_THAN:
        case GREATER_THAN:
        case LESS_THAN_EQUAL:
        case GREATER_THAN_EQUAL:
        case EQUAL:
        case NOT_EQUAL:
        case AND:
        case OR:
            m_type = INT;
            break;
        default:
            assert(false);
    }
}

// Constructor for Unary expression
Expr::Expr(Expr* lhs, Operator_type op)
{
    m_op = op;
    m_kind = EXPRESSION;
    m_left = lhs;
    m_right = NULL;

    switch (m_op)
    {
        case UNARY_MINUS:
        case ABS:
            if (m_left->get_type() == INT)
            {
                m_type = INT;
            }
            else
            {
                m_type = DOUBLE;
            }
            break;

        case NOT:
        case RANDOM:
        case FLOOR:
            m_type = INT;
            break;

        case COS:
        case SIN:
        case TAN:
        case ACOS:
        case ASIN:
        case ATAN:
        case SQRT:
            assert(m_left->get_type() != STRING);
            m_type = DOUBLE;
            break;
        default:
            assert(false);
    }

}

// Constructor for variable
Expr::Expr(Variable* var)
{   
    m_var = var;
    m_type = var->get_type();
    m_kind = VARIABLE;
    m_left = NULL;
    m_right = NULL;

    if (m_type == ANIMATION_BLOCK)
    {
        m_animation_block = m_var->get_animation_block();
    }
}

// Constructor for int constant
Expr::Expr(int val)
{
    m_type = INT;
    m_kind = CONSTANT;
    m_left = NULL;
    m_right = NULL;
    m_int_val = val;
}

// Constructor for double constant
Expr::Expr(double val)
{
    m_type = DOUBLE;
    m_kind = CONSTANT;
    m_left = NULL;
    m_right = NULL;
    m_double_val = val;
}

// Constructor for string constant
Expr::Expr(string val)
{
    m_type = STRING;
    m_kind = CONSTANT;
    m_left = NULL;
    m_right = NULL;
    m_string_val = val;
}

int Expr::eval_int()
{
    // reached a leaf node
    if (m_left == NULL && m_right == NULL)
    {
        if (m_kind == VARIABLE)
        {
            return m_var->get_int_val();
        }
        else
        {
            assert(m_kind == CONSTANT);
            return m_int_val;
        }
    }
    else if ((m_left != NULL) && (m_right == NULL)) // unary operator
    {
        Gpl_type lhs_type = m_left->get_type();
        
        // apply unary operation
        switch(m_op)
        {
            case NOT:
                if (lhs_type == INT)
                {
                    int lhs = m_left->eval_int();
                    return !lhs;
                }
                else
                {
                    double lhs = m_left->eval_double();
                    return !lhs;
                }
                break;
            case UNARY_MINUS:
                if (lhs_type == INT)
                {
                    int lhs = m_left->eval_int();
                    return -lhs;
                }
                else
                {
                    double lhs = m_left->eval_double();
                    return -lhs;
                }
                break;
            case FLOOR:
                if (lhs_type == INT)
                {
                    int lhs = m_left->eval_int();
                    return floor(lhs);
                }
                else //(lhs_type == DOUBLE)
                {
                    double lhs = m_left->eval_double();
                    return floor(lhs);
                }
                break;
            case RANDOM:
                if (lhs_type == INT)
                {
                    int lhs = m_left->eval_int();
                    return rand() % lhs;
                }
                else
                {
                    int lhs = floor(m_left->eval_double()); 
                    return rand() % lhs;
                }
                break;
            case ABS:
                if (lhs_type == INT)
                {
                    return abs(m_left->eval_int());
                }
                else
                {
                    return abs(m_left->eval_double());
                }
            default:
                assert(false);
        }           
    }
    else // binary operator 
    {
        Gpl_type lhs_type = m_left->get_type();
        Gpl_type rhs_type = m_right->get_type();

        // apply the correct operation
        switch (m_op)
        {
            case OR:
                if (lhs_type == INT && rhs_type == INT)
                {
                    return m_left->eval_int() || m_right->eval_int();
                }
                else if (lhs_type == INT && rhs_type == DOUBLE)
                {
                    return m_left->eval_int() || m_right->eval_double();
                }
                else if (lhs_type == DOUBLE && rhs_type == INT)
                {
                    return m_left->eval_double() || m_right->eval_int();
                }
                else 
                {
                    assert(lhs_type == DOUBLE && rhs_type == DOUBLE);
                    return m_left->eval_double() || m_right->eval_double();
                }
                break;
            case AND:
                if (lhs_type == INT && rhs_type == INT)
                {
                    return m_left->eval_int() && m_right->eval_int();
                }
                else if (lhs_type == INT && rhs_type == DOUBLE)
                {
                    return m_left->eval_int() && m_right->eval_double();
                }
                else if (lhs_type == DOUBLE && rhs_type == INT)
                {
                    return m_left->eval_double() && m_right->eval_int();
                }
                else 
                {
                    assert(lhs_type == DOUBLE && rhs_type == DOUBLE);
                    return m_left->eval_double() && m_right->eval_double();
                }
                break;
            case LESS_THAN_EQUAL:
                if (lhs_type == INT && rhs_type == INT)
                {
                    return m_left->eval_int() <= m_right->eval_int();
                }
                else if (lhs_type == INT && rhs_type == DOUBLE)
                {
                    return m_left->eval_int() <= m_right->eval_double();
                }
                else if (lhs_type == DOUBLE && rhs_type == INT)
                {
                    return m_left->eval_double() <= m_right->eval_int();
                }
                else if (lhs_type == DOUBLE && rhs_type == DOUBLE)
                {
                    return m_left->eval_double() <= m_right->eval_double();
                }
                else if (lhs_type == STRING && rhs_type == INT)
                {
                    int val = m_right->eval_int();
                    ostringstream s;
                    s << val;
                    string rhs = s.str();
                    return m_left->eval_string() <= rhs;
                }
                else if (lhs_type == STRING && rhs_type == DOUBLE)
                {   
                    double val = m_right->eval_double();
                    ostringstream s;
                    s << val;
                    string rhs = s.str();
                    return m_left->eval_string() <= rhs;
                }
                else if (lhs_type == STRING && rhs_type == STRING)
                {
                    return m_left->eval_string() <= m_right->eval_string();
                }
                else if (lhs_type == INT && rhs_type == STRING)
                {
                    int val = m_left->eval_int();
                    ostringstream s;
                    s << val;
                    string lhs = s.str();
                    return lhs <= m_right->eval_string();
                }
                else
                {
                    assert(lhs_type == DOUBLE && rhs_type == STRING);
                    double val = m_left->eval_double();
                    ostringstream s;
                    s << val;
                    string lhs = s.str();
                    return lhs <= m_right->eval_string();
                }
                break;
            case GREATER_THAN_EQUAL:
                if (lhs_type == INT && rhs_type == INT)
                {
                    return m_left->eval_int() >= m_right->eval_int();
                }
                else if (lhs_type == INT && rhs_type == DOUBLE)
                {
                    return m_left->eval_int() >= m_right->eval_double();
                }
                else if (lhs_type == DOUBLE && rhs_type == INT)
                {
                    return m_left->eval_double() >= m_right->eval_int();
                }
                else if (lhs_type == DOUBLE && rhs_type == DOUBLE)
                {
                    return m_left->eval_double() >= m_right->eval_double();
                }
                else if (lhs_type == STRING && rhs_type == INT)
                {
                    int val = m_right->eval_int();
                    ostringstream s;
                    s << val;
                    string rhs = s.str();
                    return m_left->eval_string() >= rhs;
                }
                else if (lhs_type == STRING && rhs_type == DOUBLE)
                {   
                    double val = m_right->eval_double();
                    ostringstream s;
                    s << val;
                    string rhs = s.str();
                    return m_left->eval_string() >= rhs;
                }
                else if (lhs_type == STRING && rhs_type == STRING)
                {
                    return m_left->eval_string() >= m_right->eval_string();
                }
                else if (lhs_type == INT && rhs_type == STRING)
                {
                    int val = m_left->eval_int();
                    ostringstream s;
                    s << val;
                    string lhs = s.str();
                    return lhs >= m_right->eval_string();
                }
                else
                {
                    assert(lhs_type == DOUBLE && rhs_type == STRING);
                    double val = m_left->eval_double();
                    ostringstream s;
                    s << val;
                    string lhs = s.str();
                    return lhs >= m_right->eval_string();
                }
                break;
            case LESS_THAN:
                if (lhs_type == INT && rhs_type == INT)
                {
                    return m_left->eval_int() < m_right->eval_int();
                }
                else if (lhs_type == INT && rhs_type == DOUBLE)
                {
                    return m_left->eval_int() < m_right->eval_double();
                }
                else if (lhs_type == DOUBLE && rhs_type == INT)
                {
                    return m_left->eval_double() < m_right->eval_int();
                }
                else if (lhs_type == DOUBLE && rhs_type == DOUBLE)
                {
                    return m_left->eval_double() < m_right->eval_double();
                }
                else if (lhs_type == STRING && rhs_type == INT)
                {
                    int val = m_right->eval_int();
                    ostringstream s;
                    s << val;
                    string rhs = s.str();
                    return m_left->eval_string() < rhs;
                }
                else if (lhs_type == STRING && rhs_type == DOUBLE)
                {   
                    double val = m_right->eval_double();
                    ostringstream s;
                    s << val;
                    string rhs = s.str();
                    return m_left->eval_string() < rhs;
                }
                else if (lhs_type == STRING && rhs_type == STRING)
                {
                    return m_left->eval_string() < m_right->eval_string();
                }
                else if (lhs_type == INT && rhs_type == STRING)
                {
                    int val = m_left->eval_int();
                    ostringstream s;
                    s << val;
                    string lhs = s.str();
                    return lhs < m_right->eval_string();
                }
                else
                {
                    assert(lhs_type == DOUBLE && rhs_type == STRING);
                    double val = m_left->eval_double();
                    ostringstream s;
                    s << val;
                    string lhs = s.str();
                    return lhs < m_right->eval_string();
                }
                break;
            case GREATER_THAN:
                if (lhs_type == INT && rhs_type == INT)
                {
                    return m_left->eval_int() > m_right->eval_int();
                }
                else if (lhs_type == INT && rhs_type == DOUBLE)
                {
                    return m_left->eval_int() > m_right->eval_double();
                }
                else if (lhs_type == DOUBLE && rhs_type == INT)
                {
                    return m_left->eval_double() > m_right->eval_int();
                }
                else if (lhs_type == DOUBLE && rhs_type == DOUBLE)
                {
                    return m_left->eval_double() > m_right->eval_double();
                }
                else if (lhs_type == STRING && rhs_type == INT)
                {
                    int val = m_right->eval_int();
                    ostringstream s;
                    s << val;
                    string rhs = s.str();
                    return m_left->eval_string() > rhs;
                }
                else if (lhs_type == STRING && rhs_type == DOUBLE)
                {   
                    double val = m_right->eval_double();
                    ostringstream s;
                    s << val;
                    string rhs = s.str();
                    return m_left->eval_string() > rhs;
                }
                else if (lhs_type == STRING && rhs_type == STRING)
                {
                    return m_left->eval_string() > m_right->eval_string();
                }
                else if (lhs_type == INT && rhs_type == STRING)
                {
                    int val = m_left->eval_int();
                    ostringstream s;
                    s << val;
                    string lhs = s.str();
                    return lhs > m_right->eval_string();
                }
                else
                {
                    assert(lhs_type == DOUBLE && rhs_type == STRING);
                    double val = m_left->eval_double();
                    ostringstream s;
                    s << val;
                    string lhs = s.str();
                    return lhs > m_right->eval_string();
                }
                break;
            case EQUAL:
                if (lhs_type == INT && rhs_type == INT)
                {
                    return m_left->eval_int() == m_right->eval_int();
                }
                else if (lhs_type == INT && rhs_type == DOUBLE)
                {
                    return m_left->eval_int() == m_right->eval_double();
                }
                else if (lhs_type == DOUBLE && rhs_type == INT)
                {
                    return m_left->eval_double() == m_right->eval_int();
                }
                else if (lhs_type == DOUBLE && rhs_type == DOUBLE)
                {
                    return m_left->eval_double() == m_right->eval_double();
                }
                else if (lhs_type == STRING && rhs_type == INT)
                {
                    int val = m_right->eval_int();
                    ostringstream s;
                    s << val;
                    string rhs = s.str();
                    return m_left->eval_string() == rhs;
                }
                else if (lhs_type == STRING && rhs_type == DOUBLE)
                {   
                    double val = m_right->eval_double();
                    ostringstream s;
                    s << val;
                    string rhs = s.str();
                    return m_left->eval_string() == rhs;
                }
                else if (lhs_type == STRING && rhs_type == STRING)
                {
                    return m_left->eval_string() == m_right->eval_string();
                }
                else if (lhs_type == INT && rhs_type == STRING)
                {
                    int val = m_left->eval_int();
                    ostringstream s;
                    s << val;
                    string lhs = s.str();
                    return lhs == m_right->eval_string();
                }
                else
                {
                    assert(lhs_type == DOUBLE && rhs_type == STRING);
                    double val = m_left->eval_double();
                    ostringstream s;
                    s << val;
                    string lhs = s.str();
                    return lhs == m_right->eval_string();
                }
                
                break;
            case NOT_EQUAL:
                if (lhs_type == INT && rhs_type == INT)
                {
                    return m_left->eval_int() != m_right->eval_int();
                }
                else if (lhs_type == INT && rhs_type == DOUBLE)
                {
                    return m_left->eval_int() != m_right->eval_double();
                }
                else if (lhs_type == DOUBLE && rhs_type == INT)
                {
                    return m_left->eval_double() != m_right->eval_int();
                }
                else if (lhs_type == DOUBLE && rhs_type == DOUBLE)
                {
                    return m_left->eval_double() != m_right->eval_double();
                }
                else if (lhs_type == STRING && rhs_type == INT)
                {
                    int val = m_right->eval_int();
                    ostringstream s;
                    s << val;
                    string rhs = s.str();
                    return m_left->eval_string() != rhs;
                }
                else if (lhs_type == STRING && rhs_type == DOUBLE)
                {   
                    double val = m_right->eval_double();
                    ostringstream s;
                    s << val;
                    string rhs = s.str();
                    return m_left->eval_string() != rhs;
                }
                else if (lhs_type == STRING && rhs_type == STRING)
                {
                    return m_left->eval_string() != m_right->eval_string();
                }
                else if (lhs_type == INT && rhs_type == STRING)
                {
                    int val = m_left->eval_int();
                    ostringstream s;
                    s << val;
                    string lhs = s.str();
                    return lhs != m_right->eval_string();
                }
                else
                {
                    assert(lhs_type == DOUBLE && rhs_type == STRING);
                    double val = m_left->eval_double();
                    ostringstream s;
                    s << val;
                    string lhs = s.str();
                    return lhs != m_right->eval_string();
                }
                break;
            case PLUS:
                assert(lhs_type == INT && rhs_type == INT);
                return m_left->eval_int() + m_right->eval_int();
                break;
            case MINUS:
                assert(lhs_type == INT && rhs_type == INT);
                return m_left->eval_int() - m_right->eval_int();
                break;
            case MULTIPLY:
                assert(lhs_type == INT && rhs_type == INT);
                return m_left->eval_int() * m_right->eval_int();
                break;
            case DIVIDE:    
                assert(lhs_type == INT && rhs_type == INT);
                return m_left->eval_int() / m_right->eval_int();
                break;
            case MOD:
                assert(lhs_type == INT && rhs_type == INT);
                return m_left->eval_int() % m_right->eval_int();
                break;
            default:
                assert(false);
        }
    }
}

double Expr::eval_double()
{
    
    // reached a leaf node
    if (m_left == NULL && m_right == NULL)
    {
        if (m_kind == VARIABLE)
        {
            return m_var->get_double_val();
        }
        else
        {
            assert(m_kind == CONSTANT);
            return m_double_val;
        }
    }
    else if ((m_left != NULL) && (m_right == NULL)) // unary operator
    {   
        Gpl_type lhs_type = m_left->get_type();

        // apply unary operation
        switch(m_op)
        {
            case UNARY_MINUS: // accepts int,double
                if (lhs_type == INT)
                {
                    return -m_left->eval_int();
                }
                else
                {
                    return -m_left->eval_double();
                }
                break;
            case COS:
                if (lhs_type == INT)
                {
                    return cos(PI/180.0 * m_left->eval_int());
                }
                else
                {
                    return cos(PI/180.0 * m_left->eval_double());
                }
                break;
            case SIN:
                if (lhs_type == INT)
                {
                    return sin(PI/180.0 * m_left->eval_int());
                }
                else
                {
                    return sin(PI/180.0 * m_left->eval_double());
                }
                break;
            case TAN:
                if (lhs_type == INT)
                {
                    return tan(PI/180.0 * m_left->eval_int());
                }
                else
                {
                    return tan(PI/180.0 * m_left->eval_double());
                }
                break;
            case ACOS:
                if (lhs_type == INT)
                {
                    return acos(m_left->eval_int()) * 180.0/PI;
                }
                else
                {
                    return acos(m_left->eval_double()) * 180.0/PI;
                }
                break;
            case ASIN:
                if (lhs_type == INT)
                {
                    return asin(m_left->eval_int()) * 180.0/PI;
                }
                else
                {
                    return asin(m_left->eval_double()) * 180.0/PI;
                }
                break;
            case ATAN:
                if (lhs_type == INT)
                {
                    return atan(m_left->eval_int()) * 180.0/PI;
                }
                else
                {
                    return atan(m_left->eval_double()) * 180.0/PI;
                }
                break;
            case SQRT:
                if (lhs_type == INT)
                {
                    return sqrt(m_left->eval_int());
                }
                else
                {
                    return sqrt(m_left->eval_double());
                }
                break;
            case ABS:
                if (lhs_type == INT)
                {
                    return abs(m_left->eval_int());
                }
                else
                {
                    return abs(m_left->eval_double());
                }
                break;
            case FLOOR:
                if (lhs_type == INT)
                {
                    return floor(m_left->eval_int());
                }
                else
                {
                    return floor(m_left->eval_double());
                }
                break;
            default:
                assert(false);
        }
    }
    else // binary operation
    {
        Gpl_type rhs_type = m_right->get_type(); 
        Gpl_type lhs_type = m_left->get_type();


        // apply binary operation
        switch(m_op)
        {
            case MULTIPLY:
                if (lhs_type == DOUBLE && rhs_type == DOUBLE)
                {
                    return m_left->eval_double() * m_right->eval_double();
                }
                else if (lhs_type == INT && rhs_type == DOUBLE)
                {
                    return m_left->eval_int() * m_right->eval_double();
                }
                else
                {
                    assert(lhs_type == DOUBLE && rhs_type == INT);
                    return m_left->eval_double() * m_right->eval_int();
                }                
                break;
            case DIVIDE:
                if (lhs_type == DOUBLE && rhs_type == DOUBLE)
                {
                    return m_left->eval_double() / m_right->eval_double();
                }
                else if (lhs_type == INT && rhs_type == DOUBLE)
                {
                    return m_left->eval_int() / m_right->eval_double();
                }
                else
                {
                    assert(lhs_type == DOUBLE && rhs_type == INT);
                    return m_left->eval_double() / m_right->eval_int();
                }
                break;
            case PLUS:
                if (lhs_type == DOUBLE && rhs_type == DOUBLE)
                {
                    return m_left->eval_double() + m_right->eval_double();
                }
                else if (lhs_type == INT && rhs_type == DOUBLE)
                {
                    return m_left->eval_int() + m_right->eval_double();
                }
                else
                {
                    assert(lhs_type == DOUBLE && rhs_type == INT);
                    return m_left->eval_double() + m_right->eval_int();
                }
                break;
            case MINUS:
                if (lhs_type == DOUBLE && rhs_type == DOUBLE)
                {
                    return m_left->eval_double() - m_right->eval_double();
                }
                else if (lhs_type == INT && rhs_type == DOUBLE)
                {
                    return m_left->eval_int() - m_right->eval_double();
                }
                else
                {
                    assert(lhs_type == DOUBLE && rhs_type == INT);
                    return m_left->eval_double() - m_right->eval_int();
                }
                break;
            default:
                assert(false);
        }
    }
}

string Expr::eval_string()
{

    // reached a leaf node
    if (m_left == NULL && m_right == NULL)
    {
        if (m_kind == VARIABLE)
        {
            return m_var->get_string_val();
        }
        else
        {
            assert(m_kind == CONSTANT);
            return m_string_val;
        }
    }
    else if ((m_left != NULL) && (m_right == NULL)) // unary op
    {
        assert(false);
    }
    else
    {
        
        Gpl_type rhs_type = m_right->get_type(); 
        Gpl_type lhs_type = m_left->get_type();
        
        switch (m_op)
        {
            case PLUS:
                if (lhs_type == STRING && rhs_type == STRING)
                {
                    string string_lhs = m_left->eval_string();
                    string string_rhs = m_right->eval_string();
                    return string_lhs + string_rhs;
                }
                else if (lhs_type == STRING && rhs_type == INT)
                {
                    string string_lhs = m_left->eval_string();
                    int int_rhs = m_right->eval_int();
                    
                    ostringstream int_val;
                    int_val << int_rhs;
                    
                    return string_lhs + int_val.str();
                }
                else if (lhs_type == STRING && rhs_type == DOUBLE)
                {
                    string string_lhs = m_left->eval_string();
                    double double_rhs = m_right->eval_double();

                    ostringstream double_val;
                    double_val << double_rhs;


                    return string_lhs + double_val.str();
                }
                else if (lhs_type == INT && rhs_type == STRING)
                {
                    int int_lhs = m_left->eval_int();
                    string string_rhs = m_right->eval_string();

                    ostringstream int_val;
                    int_val << int_lhs;

                    return int_val.str() + string_rhs;
                }
                else
                {
                    double double_lhs = m_left->eval_double();
                    string string_rhs = m_right->eval_string();
                    
                    ostringstream double_val;
                    double_val << double_lhs;

                    return double_val.str() + string_rhs;
                }
                break;
            default:
                assert(false);
         }
     }
}

Animation_block* Expr::eval_animation_block()
{
    assert(m_kind == VARIABLE);
    assert(m_left == NULL && m_right == NULL);
    assert(m_animation_block != NULL);

    return m_animation_block;
}
