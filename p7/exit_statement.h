#ifndef EXIT_STATEMENT_H
#define EXIT_STATMENT_H

#include "statement.h"
#include "expression.h"

class ExitStmt : public Statement
{
    public:
        ExitStmt(Expr* expr, int line_no);
        
        virtual void execute();

    private:
        Expr *m_expr;
        int m_line_no;

};

#endif
