#include "if_statement.h"

IfStmt::IfStmt(Expr* expr, Statement_block* then_block, Statement_block* else_block)
{
    m_expr = expr;
    m_then_block = then_block;
    m_else_block = else_block;
}

void IfStmt::execute()
{
    if(m_expr->eval_int())
    {
        m_then_block->execute();
    }
    else
    {
        // if there's an else block, then execute it
        if(m_else_block != NULL)
        {
            m_else_block->execute();
        }
    }
}
