#ifndef EXPRESSION_H
#define EXPRESSION_H

#include "gpl_type.h"
#include "variable.h"
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <sstream>

#define PI 3.14159265

class Variable;

class Expr
{
    public:
        enum Node_kind {CONSTANT,
                        EXPRESSION,
                        VARIABLE};
        
        Expr(Expr* lhs, Expr* rhs, Operator_type op);
        Expr(Expr* lhs, Operator_type op);
        Expr(Variable* var);
        Expr(int val);
        Expr(double val);
        Expr(string val);         
        
        int     eval_int();
        double  eval_double();
        string  eval_string();

        Gpl_type get_type() {return m_type;}
 
    private:
        Gpl_type        m_type;
        int             m_kind;
        Expr           *m_left;
        Expr           *m_right;
        int             m_int_val;
        double          m_double_val;
        string          m_string_val;
        Operator_type   m_op;
        Variable        *m_var;



};

#endif
