#ifndef IFSTATEMENT_H
#define IFSTATEMENT_H

#include "statement.h"
#include "statement_block.h"
#include "expression.h"

class Expression;
class Statement_block;

class IfStmt : public Statement
{
    public:
        IfStmt(Expr *expr, 
                    Statement_block* then_block, 
                    Statement_block* else_block = NULL );

        virtual void execute();
    
    private:
        Expr                *m_expr;
        Statement_block     *m_then_block;
        Statement_block     *m_else_block;



};

#endif
