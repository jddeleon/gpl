#include "assignment_statement.h"
#include "variable.h"

AssignmentStmt::AssignmentStmt(Variable *lhs, Expr *rhs, Operator_type op)
{
    m_lhs = lhs;
    m_rhs = rhs;
    m_op = op;
}

void AssignmentStmt::execute()
{
    assert(m_rhs != NULL);
    
    Gpl_type expr_type = m_rhs->get_type();
    Gpl_type var_type = m_lhs->get_type();
    
    /* need to update the symbol, but my variable constructors don't all set
     * the symbol* so now I need to change the variable class to make sure the
     * symbol can be retrieved.*/
    Symbol* var_symbol = m_lhs->get_symbol();
    assert(var_symbol != NULL);
    Variable::Var_kind var_kind = m_lhs->get_kind();
    Game_object* obj;
    string member_id;

    if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
    {
        obj = var_symbol->get_game_object_val();
        member_id = m_lhs->get_member_id();    
    }
    if (m_op == ASSIGN)
    {
        if (var_type == INT)
        {
            if (expr_type == INT)
            {
                var_symbol->set_int_val(m_rhs->eval_int());
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                   obj->set_member_variable(member_id, m_rhs->eval_int());
                }

            }
            else if (expr_type == DOUBLE)
            {
                // doubles cannot be cast to integers
                assert(false);
            }
            else if (expr_type == STRING)
            {
                // strings cannot be cast to integers
                assert(false);
            }
            else
            {
                assert(expr_type == GAME_OBJECT);
            }
        }
        else if (var_type == DOUBLE)
        {
            if (expr_type == INT)
            {
                var_symbol->set_double_val(m_rhs->eval_int());
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                   obj->set_member_variable(member_id, m_rhs->eval_int());
                }
            }
            else if (expr_type == DOUBLE)
            {
                var_symbol->set_double_val(m_rhs->eval_double());
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                   obj->set_member_variable(member_id, m_rhs->eval_double());
                }
            }
            else if (expr_type == STRING)
            {
                // strings cannot be cast to doubles
                assert(false);
            }
            else
            {
                assert(expr_type == GAME_OBJECT);
            }
        }
        else if (var_type == STRING)
        {
            if (expr_type == INT)
            {
                int val = m_rhs->eval_int();
                ostringstream s;
                s << val;
                string str_val = s.str();

                var_symbol->set_string_val(str_val);
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                   obj->set_member_variable(member_id, m_rhs->eval_int());
                }
            }
            else if (expr_type == DOUBLE)
            {
                double val = m_rhs->eval_double();
                ostringstream s;
                s << val;
                string str_val = s.str();

                var_symbol->set_string_val(str_val);
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                    obj->set_member_variable(member_id, m_rhs->eval_double());
                }
            }
            else if (expr_type == STRING)
            {
                var_symbol->set_string_val(m_rhs->eval_string());
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                    obj->set_member_variable(member_id, m_rhs->eval_string());
                }
            }
            else
            {
                assert(expr_type == GAME_OBJECT);
            }
        }
        else
        {
            assert(var_type == GAME_OBJECT);
            if (expr_type == INT)
            {
            }
            else if (expr_type == DOUBLE)
            {
            }
            else if (expr_type == STRING)
            {
            }
            else
            {
                assert(expr_type == GAME_OBJECT);
            }
        }
    
    }
    else if (m_op == PLUS_ASSIGN)
    {
        if (var_type == INT)
        {
            int var_value = var_symbol->get_int_val();

            if (expr_type == INT)
            {
                var_symbol->set_int_val(var_value + m_rhs->eval_int());
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                    obj->set_member_variable(member_id, m_rhs->eval_int() + var_value);
                }
            }
            else if (expr_type == DOUBLE)
            {
                // doubles cannot be cast to integers
                assert(false);
            }
            else if (expr_type == STRING)
            {
                // strings cannot be cast to integers
                assert(false);
            }
            else
            {
                assert(expr_type == GAME_OBJECT);
            }
        }
        else if (var_type == DOUBLE)
        {
            double var_value = var_symbol->get_double_val();

            if (expr_type == INT)
            {
                var_symbol->set_double_val(var_value + m_rhs->eval_int());
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                    obj->set_member_variable(member_id, m_rhs->eval_int() + var_value);
                }
            }
            else if (expr_type == DOUBLE)
            {
                var_symbol->set_double_val(var_value + m_rhs->eval_double());
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                    obj->set_member_variable(member_id, m_rhs->eval_double() + var_value);
                }
            }
            else if (expr_type == STRING)
            {
                // strings cannot be cast to doubles
                assert(false);
            }
            else
            {
                assert(expr_type == GAME_OBJECT);
            }
        }
        else if (var_type == STRING)
        {
            string var_value = var_symbol->get_string_val();

            if (expr_type == INT)
            {
                int val = m_rhs->eval_int();
                ostringstream s;
                s << val;
                string str_val = s.str();

                var_symbol->set_string_val(var_value + str_val);
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                    obj->set_member_variable(member_id, var_value + str_val);
                }
            }
            else if (expr_type == DOUBLE)
            {
                double val = m_rhs->eval_double();
                ostringstream s;
                s << val;
                string str_val = s.str();

                var_symbol->set_string_val(var_value + str_val);
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                    obj->set_member_variable(member_id, var_value + str_val);
                }
            }
            else if (expr_type == STRING)
            {
                var_symbol->set_string_val(var_value + m_rhs->eval_string());
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                    obj->set_member_variable(member_id, var_value + m_rhs->eval_string());
                }
            }
            else
            {
                assert(expr_type == GAME_OBJECT);
            }
        }
        else
        {
            assert(var_type == GAME_OBJECT);
            if (expr_type == INT)
            {
            }
            else if (expr_type == DOUBLE)
            {
            }
            else if (expr_type == STRING)
            {
            }
            else
            {
                assert(expr_type == GAME_OBJECT);
            }
        }
    }
    else
    {
        assert(m_op == MINUS_ASSIGN);
        if (var_type == INT)
        {
            int var_value = var_symbol->get_int_val();

            if (expr_type == INT)
            {
                var_symbol->set_int_val(var_value - m_rhs->eval_int());
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                    obj->set_member_variable(member_id, var_value - m_rhs->eval_int());
                }
            }
            else if (expr_type == DOUBLE)
            {
                // not sure if this is legal
                var_symbol->set_int_val(var_value - m_rhs->eval_double());
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                    obj->set_member_variable(member_id, var_value - m_rhs->eval_double());
                }
            }
            else if (expr_type == STRING)
            {
                // strings cannot be cast to integers
                assert(false);
            }
            else
            {
                assert(expr_type == GAME_OBJECT);
            }
        }
        else if (var_type == DOUBLE)
        {
            double var_value = var_symbol->get_double_val();

            if (expr_type == INT)
            {
                var_symbol->set_double_val(var_value - m_rhs->eval_int());
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                    obj->set_member_variable(member_id, var_value - m_rhs->eval_int());
                }
            }
            else if (expr_type == DOUBLE)
            {
                var_symbol->set_double_val(var_value - m_rhs->eval_double());
                
                if (var_kind == Variable::MEMBER || var_kind == Variable::MEMBER_ARRAY)
                { 
                    obj->set_member_variable(member_id, var_value - m_rhs->eval_double());
                }
            }
            else if (expr_type == STRING)
            {
                // strings cannot be cast to doubles
                assert(false);
            }
            else
            {
                assert(expr_type == GAME_OBJECT);
            }
        }
        else if (var_type == STRING)
        {
            // not legal to subtract strings
            assert(false);
        }
        else
        {
            assert(var_type == GAME_OBJECT);
            if (expr_type == INT)
            {
            }
            else if (expr_type == DOUBLE)
            {
            }
            else if (expr_type == STRING)
            {
            }
            else
            {
                assert(expr_type == GAME_OBJECT);
            }
        }
        
    }
}
