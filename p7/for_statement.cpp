#include "for_statement.h"

ForStmt::ForStmt(Statement_block* initializer, Statement_block* incrementor, 
                 Statement_block* body, Expr* expr)
{
    m_initializer = initializer;
    m_incrementor = incrementor;
    m_body = body;
    m_expr = expr;
}

void ForStmt::execute()
{
    for (m_initializer->execute(); m_expr->eval_int(); m_incrementor->execute())
    {
        m_body->execute();
    }
 
}

